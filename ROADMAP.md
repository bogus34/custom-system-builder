# Planned Features

## Labels

- 🚧️ : In progress
- ✔️ : Done waiting for release
- 💤️ : Waiting for development
- ❌️ : Postponed / Aborted

## Planned for 3.1.0

- ✔️ [#280] Labels executing formulas without chat message
- ✔️ [#180] Make Item Container Referencable
- ❌️ [#198] Items in Items
- ❌️ [#141] Custom Active Effects
- ✔️ [#232] Collapsible Panels
