/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

module.exports = {
    'Create Actor': '.actors-sidebar button.create-document',
    'Actor name': '//input[@name="name"]',
    'Actor type': '//select[@name="type"]',
    'Create New Actor': '.dialog-button.ok',

    'Advanced configuration': 'div.custom-system-collapsible-block-title.advanced-configuration',
    'Save Component': 'button.dialog-button.validate',
    'Delete Component': 'button.dialog-button.delete',
    'Cancel Component Edition': 'button.dialog-button.cancel'
};
