/* eslint-disable no-undef */
import packageJson from './package.json' with { type: "json" };
import system from './system.json' with { type: "json" };
import fs from 'fs';

if(process.argv.length !== 3){
    console.error('This script needs one argument : the new version')
    process.exit(1)
}

const newVersion = process.argv[2];

packageJson.version = newVersion

system.version = newVersion
system.download = `https://gitlab.com/custom-system-builder/custom-system-builder/-/releases/${newVersion}/downloads/custom-system-builder.zip`

fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 4))
fs.writeFileSync('./system.json', JSON.stringify(system, null, 4))

let changelogContents = fs.readFileSync('./CHANGELOG.md', 'utf-8');
changelogContents = changelogContents.replace(new RegExp(`## ${newVersion} - UNRELEASED`), `## ${newVersion}`);
fs.writeFileSync('./CHANGELOG.md', changelogContents)


try {
    fs.rmSync('./README.md')
} catch(err){
    console.error('Error when deleting README', err)
}

let readmeContents = fs.readFileSync('./README_BETA.md', 'utf-8');
readmeContents = readmeContents.replace(/docs_beta/g, 'docs');
fs.writeFileSync('./README.md', readmeContents)

try {
    await new Promise((resolve, reject) => {fs.rm('./docs', { recursive: true, force: true }, (err) => {
        if(err){
            reject(err);
        } else {
            resolve();
        }
    })});
} catch(err){
    console.error('Error when deleting docs folder', err)
}

fs.cpSync('./docs_beta/', './docs/', { recursive: true });