TROIKA! System for Foundry VTT v10 by Tak5haka

This is not an official system, it is an independent production and I am not affiliated with the Melsonian Arts Council. The turn tracker and generator is an independent production by Technical Grimoire.
This system contains no content, other than templates for Character Sheets (including base functionality such as dice rolls, item tracking, etc), Enemy Sheets and Items with Damage (so that the damage table can be generated for anything that does damage).

For use with the Custom System Builder system with the following settings:

- Initiative Formula: Blank (do not use).
- CSS Style file: Upload the "Foundry_Troika_CSS_Tak5haka.css" file, or use the Custom CSS module.
- Roll Icons: use "dice".

Other settings are up to you.

REQUIRED MODULES:

- Inline Webviewer (used for the TROIKA! Initiative System until I can work out a native solution) with the following settings for a Private Webviewer:
	- Name: Turn Tracker
	- Url: https://www.technicalgrimoire.com/troikagenerator#turn-tracker
	- Icon: fa-regular fa-cards-blank

RECOMMENDED MODULES:
- Dice So Nice!
- Pin Cushion
- PopOut!
- Lock View
- Ownership Viewer

DEPENDENCY MODULES:
- lib - Color Settings
- libWrapper
- socketlib
