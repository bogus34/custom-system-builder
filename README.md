<!-- markdownlint-disable MD036 MD040 MD046 MD033 -->

# Custom System Builder

[![Forge install count](https://img.shields.io/badge/dynamic/json?label=Forge%20Installs&query=package.installs&suffix=%25&url=https%3A%2F%2Fforge-vtt.com%2Fapi%2Fbazaar%2Fpackage%2Fcustom-system-builder&colorB=success&style=flat)](https://forge-vtt.com/bazaar#package=custom-system-builder)

Build your custom system without any line of code!

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Z8Z6AJTSY)

> &#x26a0;&#xfe0f; **Formula changes**
>
> The formula syntax has changed and some specific syntax has been removed with v3.0.0. Here is a summary of the changes:
>
> - `$<colName>` becomes `sameRow('<colName>')`
> - `<dynamicTableKey>(<columnName>=<filterValue>)$<targetColumn>` becomes `lookup('<dynamicTableKey>', '<targetColumn>', '<columnName>', '<filterValue>')`
> - `<dynamicTableKey>$<targetColumn>` becomes `lookup('<dynamicTableKey>', '<targetColumn>')`
> - `@<property>` becomes `ref(<property>)`
> - `@$<colName>` becomes `ref(sameRow('<colName>'))`
>
> [More information on the new syntax here](#42-additional-functions).

> &#x26a0;&#xfe0f; **Formula deprecations**
>
> The following functions have been deprecated in 3.1.0 and will be removed in 4.0.0:
>
> - `fetchFromDynamicTable`: You should now use the `lookup` function. No change in usage.
> - `getRefFromDynamicTable`: You should now use the `lookupRef` function. No change in usage.
>
> [More information on the new syntax here](#42-additional-functions).

## Introduction

This system is intended to easily build sheets in FoundryVTT with dynamic display and roll capabilities, in a game agnostic way.

You can find the Wiki-page of this project [here](https://gitlab.com/custom-system-builder/custom-system-builder/-/wikis/home). It contains guidelines, tips & tricks and some other stuff. It's worth a visit.

If you have any issues, requests or questions, please report them [here](https://gitlab.com/custom-system-builder/custom-system-builder/-/issues/new). You can also ask on the [FoundryVTT Discord](https://discord.gg/foundryvtt) ([Official CSB Channel](https://discord.com/channels/170995199584108546/1037072885044477962)), we are `@LinkedFluuuush [CSB Creator]` and `@Martin1522 [CSB Dev]`!

For an up-to-date Roadmap, please visit [ROADMAP.md](https://gitlab.com/custom-system-builder/custom-system-builder/-/blob/develop/ROADMAP.md) on the develop branch.

If you want to contribute to the project, please see [CONTRIBUTING.md](https://gitlab.com/custom-system-builder/custom-system-builder/-/blob/main/CONTRIBUTING.md).

If you seek to extend the system or use it in macros, you can [visit the API](https://custom-system-builder.gitlab.io/custom-system-builder/).

If you like this system and want to support me, you can [buy me a coffee](https://ko-fi.com/linkedfluuuush) ! You can also send us kind words through the Discord channel or by creating an issue, they are always appreciated !

## How to install the system

You can install the latest released version of the system by using the package manager in your FoundryVTT server! Alternatively, you can use this manifest link to install the system : `https://gitlab.com/custom-system-builder/custom-system-builder/-/raw/main/system.json`.

If you want to test the next version ahead of time, you can have access to the beta version with this manifest link : `https://gitlab.com/api/v4/projects/31995966/jobs/artifacts/beta/raw/out/system.json?job=build-beta`. The specific doc for the beta version is available here : [README_BETA.md](https://gitlab.com/custom-system-builder/custom-system-builder/-/blob/beta/README_BETA.md)

If you want to switch an existing world to the beta version, you can [follow this guide !](https://gitlab.com/custom-system-builder/custom-system-builder/-/wikis/Guides/Switch-to-Beta)

> &#x26a0;&#xfe0f; **Unstable version**
>
> Lastly, if you want to see the latest changes, at the risk of breaking your games, you can have access to the unstable version with this manifest link : `https://gitlab.com/api/v4/projects/31995966/jobs/artifacts/develop/raw/out/system.json?job=build-unstable`.
>
> Be aware that this version reflects the latest code in this repository, and thus can contain untested development, or breaking code. Please back-up your games regularly if you plan to use this version!

## Sheet library

Seeking for existing templates working with the Custom System Builder? [Look here if you can find something suiting you!](https://gitlab.com/custom-system-builder/custom-system-builder/-/blob/develop/sheet-library). There is also an [Example](https://gitlab.com/custom-system-builder/custom-system-builder/-/blob/develop/sheet-library/Example) in the library, which utilizes most features in CSB. It might help you to start with the system if you're new.

If you made sheets for Custom System Builder and would like to add them to the library, please send them to us by Discord or by creating a new ticket in the Issue-Tab!

## Incompatibilities

- This system tinkers with initiative rolls and calculations, it might cause problems with modules modifying initiative as well.

## Index

[[_TOC_]]

## 1. Before we start

This system defines two Actor types: Templates (`_template`) and Characters (`character`). The `_template` type is for sheet-building while the `character` type uses a template to represent a Character. Templates can not be filled out, and are not supposed to be. Their sole purpose is to build and edit sheet structures, in an easy-to-use way. Character sheets are where the magic happens. Just imagine, that Templates are a blueprint, while Characters are an actual building.

CSB-Sheets are divided into 2 main parts:

- The header, located under the actor's name and to the right of the character's profile picture
- The body, under both the header and the profile picture

![Sheet sections](docs/imgs/sheet-sections.png)

The header and the body section both act in the same way, which is why they will be covered by the same section of this doc.

The term `key` is used many times in this doc. It refers to an identifier of fields or tabs. It must always be a string composed of letters (upper and lowercase), numbers and underscores only. Keys must be unique throughout the sheet.

## 2. Creating a basic sheet

To create a basic sheet, you will want to create a `_template` type actor first. The name of the Template should be unique, so that you won't get issues when trying to link a Template with a Character. Once your `_template` is created, the sheet will open and it will look like this:

![Blank _template sheet](docs/imgs/blank-template.png)

### 2.1. Adding components to body and header

To add a component to the sheet structure, click the `+` icon in the section you want to add the component in. It will open a pop-up dialog asking for details for your new component.

![Add new component](docs/imgs/new-label.png)

You can start by selecting the component type you want to add. If you're unsure what a specific component is doing, you can always [check this section](#3-component-library), in which all components are documented. For most components a key is mandatory. It will be used for various purposes, including saving the current value of the component under `system.props.<keyOfComponent>` and HTML-classes added to the component in the resulting sheets. Once you've finished the configuration, the component will be displayed on the Template. You can open the component configuration window again by clicking on the created component, in there you can perform changes or simply delete the whole component.

If you want to adjust the styling of your Components, you should check our section for [using your own CSS](#84-use-your-own-css).

To move the component in the sheet, you can drag and drop it where you want it to be. The component will be placed before the component you drag it onto, or at the end of the container if you drag it onto a + button. If you hold the CTRL-key while dragging, the component will be copied instead of being moved.

![Move component](docs/imgs/drag_drop.gif)

### 2.2. Creating a character

When your template is complete, you can associate it to a character. Create an actor with type `character`, and its sheet will open, looking like this.

![Blank character sheet](docs/imgs/blank-character.png).

On the top right of the sheet, a drop-down list allows you to select the template to use, and a button which applies the template. If you modify your template, you will need to click this button again to update the character sheet. This prevents any mistake deleting fields from the character sheets. This action is only available to GMs and Assistants by default, but can be changed in the settings.

The reload action is also available in the context menu of the character in the actor directory on the right side of your window, though it just reloads the current template and does not allow for template changing.

## 3. Component library

**REMINDER :** The key is sometimes optional, but if set it MUST be unique throughout the whole sheet to work properly. The key can only be composed of letters (upper and lowercase), numbers and underscores.

Each component can have a tooltip, which is a little text displayed when hovering the mouse over the component for some time. You can also use [formulas](#4-formulas).

Under the key and tooltip fields is an advanced configuration section, displayable by clicking on the Advanced configuration label. This section allows for more configuration options for the components.

![Component's advanced configuration section](docs/imgs/component_advanced_configuration.png)

Each component can be limited by the current player role and permission. To see a component, the user's role AND permission on the sheet must be above or equal to the minimum role and permission configured on the element. This allows you to create GM-Only Components or Tabs, and limit the view of non-owner players on their companions' sheets for example.

Each component can be hidden depending on a [formulas](#4-formulas) result. This formula must not be enclosed in the `${}$` brackets, and it will only show the component if it resolves to `true`

You can add CSS classes on every component, which will be added to the rendered component on the sheet. See [here](#84-use-your-own-css) for more information on CSS enhancing.

### 3.1. Label

Labels are simple text rendered in the sheet. They can be dynamic, and they are able to trigger chat messages (including rolls).

![Label configuration dialog](docs/imgs/new-label.png)

- **Key** (optional): If set, allows the label text to be re-used in [formulas](#4-formulas) by referring to the key.
- **Label style**: You can choose the style of the label between title, subtitle, bold text, button and default. This will only affect the label's rendering.
- **Size**: Specifies the size of the field. 4 options are available :
  - **Auto**: The field will take all the available space, resizing automatically.
  - **Small / Medium / Large**: The field will have a fixed size, depending on your choice.
- **Label text**: The label's displayed text. Can use [formulas](#4-formulas).
- **Label prefix**: An optional prefix for the label. Is not used in the label's saved value. Can use [formulas](#4-formulas).
- **Label suffix**: An optional suffix for the label. Is not used in the label's saved value. Can use [formulas](#4-formulas).
- **Label icon**: You can add an icon in front of the label. Use a valid [fontawesome](https://fontawesome.com) icon name, like `dice-d20`.
- **Label roll message**: If set, will compute this text on a click on the Label. Can use [formulas](#4-formulas).
- **Send roll message to chat**: If checked, the roll message will be sent to the chat after computation. Can be unchecked if the roll message is a utility function.
- **Label alternative roll message**: If set, will compute this text on a click on the Label with Shift key pressed. Can use [formulas](#4-formulas). This allows for a standard roll to be set and an advanced one for special cases.
- **Send alternative roll message to chat**: If checked, the alternative roll message will be sent to the chat after computation. Can be unchecked if the alternative roll message is a utility function.

### 3.2. Text field

Text fields are one-lined fields to input any string you want.

![Text field configuration dialog](docs/imgs/new-text-field.png)

- **Key**: Mandatory - will not save otherwise
- **Label**: If set, will display this label next to the field on the sheet
- **Size**: Specifies the size of the field. 4 options are available:
  - **Auto**: The field will take all the available space, resizing automatically.
  - **Small / Medium / Large**: The field will have a fixed size, depending on your choice.
- **Allowed character list**: Specifies a list of characters authorized in the field. Inputs with other characters will be rejected.
- **Maximum length**: Sets the maximum number of characters allowed in the text field.
- **Default value**: If set, the field will have this value by default when the component is getting created
- **Autocomplete formula**: If set, the text field will provide autocompletion options on input, without enforcing them. The field should contain the groups names, separated by commas `,`. It can contain Formulas.

### 3.3. Number field

Number fields are one-lined fields to input numbers.

![Number field configuration dialog](docs/imgs/new-number-field.png)

- **Key**: Mandatory - will not save otherwise
- **Label**: If set, will display this label next to the field on the sheet
- **Size**: Specifies the size of the field. 4 options are available:
  - **Auto**: The field will take all the available space, resizing automatically.
  - **Small / Medium / Large**: The field will have a fixed size, depending on your choice.
- **Allow decimal number**: Allows decimal numbers to be entered. Accepted numbers are integer only by default.
- **Minimum value**: Sets the minimum value (inclusive) accepted in the field. This can be a [formula](#4-formulas).
- **Maximum value**: Sets the maximum value (inclusive) accepted in the field. This can be a [formula](#4-formulas). If a maximum value is set, the system will automatically create an Attribute Bar, which can be selected in the configuration of the Token
- **Default value**: If set, the field will have this value by default when the component is getting created
- **Allow relative modification**: If set, when a player enters `+x` or `-y`, the operation will be done relative to the current value of the field instead of directly setting the new value.
- **Show field controls**: If set, Buttons next to the field will be displayed to edit its value, based on the `Controls style`-setting.
- **Controls style**: Choose the style you want for the controls
  - **Hover to display**: Number field will have a `+` and `-` button shown when mouse is over them to increment and decrement the value.
  - **Full controls**: Number field will have `-10`, `-1`, `+1`, `+10` buttons around it, always displayed, to edit its value by the indicated amount.
- **Input style**: Choose the style you want for the number field
  - **Display as a text field**: Displays as a standard text field.
  - **Display as a slider**: Displays as a range selector, like a volume slider.

### 3.4. Rich text area

Rich text areas are fields which can contain enriched HTML data. They come with a WYSIWYG-editor.

![Rich text area configuration dialog](docs/imgs/new-rta.png)

- **Key**: Mandatory - will not save otherwise
- **Label**: If set, will display this label next to the field on the sheet
- **Default value**: If set, the field will have this value by default
- **Style**: Three styles are available
  - **In sheet editor**: Rich text will be displayed on the sheet, with a button to open the editor directly within the sheet
  - **Dialog editor**: Rich text will be displayed on the sheet, with a button to open a pop-up dialog containing the editor
  - **Icon only**: Only an icon to open a pop-up dialog containing the editor will be displayed on the sheet. Useful for tight spaces

### 3.5. Checkbox

Checkboxes are checkboxes. They return either `true` or `false`. Sometimes it's that simple.

![Checkbox configuration dialog](docs/imgs/new-checkbox.png)

- **Key**: Mandatory - will not save otherwise
- **Label**: If set, will display this label next to the checkbox on the sheet
- **Size**: Specifies the size of the field. 4 options are available:
  - **Auto**: The field will take all the available space, resizing automatically.
  - **Small / Medium / Large**: The field will have a fixed size, depending on your choice.
- **Checked by default**: The checkbox will be checked on new sheets

### 3.6. Radio buttons

Radio buttons are checkboxes in a group, where only one can be selected at a time. They return the value of the selected one.

**WARNING :** Contrary to other components, the value of a radio buttons group is obtained in Formulas by using the name of the group, not the key to the radio buttons.

![Radio button configuration dialog](docs/imgs/new-radioButton.png)

- **Key**: Mandatory - will not save otherwise
- **Label**: If set, will display this label next to the radio button on the sheet
- **Size**: Specifies the size of the field. 4 options are available:
  - **Auto**: The field will take all the available space, resizing automatically.
  - **Small / Medium / Large**: The field will have a fixed size, depending on your choice.
- **Group**: The group of radio buttons this button belongs to. Only one radio button of this group can be selected at a time. The group is also the key to use in Formulas. Mandatory - will not save otherwise
- **Value**: This radio button's value when selected. This can contain [formulas](#4-formulas).
- **Checked by default**: Whether this radio button should be checked by default in the sheet. If multiple radio buttons are to be checked by default in a single group, only the last one will really be checked in the character's sheets.

### 3.7. Drop-down list

Drop-down lists are fields with limited options.

![Drop down configuration dialog](docs/imgs/new-dropdown.png)

- **Key**: Mandatory - will not save otherwise
- **Label**: If set, will display this label next to the field on the sheet
- **Options to use from**: You can select how you want to populate the options in the dropdown-list.
  - **Custom**: Manually enter your options
  - **Dynamic Table**: Fetch options from Dynamic Table-columns
  - **Formula**: Fetch options from formulas.
- **Options**: The value list displayed in the sheet. Enter one key and one Label per row. You can only reference the value of the key in formulas, it´s not possible to do so with the Label. But contrary to component keys, every character is allowed (including whitespaces).
- **Dynamic table key**: The source dynamic table's key. In items, it is possible to reference a parent's dynamic table by adding the `parent.` prefix in front of the key.
- **Column from the dynamic table to use as option key**: The key of the column in the dynamic table to use as options keys.
- **Column from the dynamic table to use as option label (optional)**: The key of the column in the dynamic table to use as options labels.
- **Formula for Key-Options**: The [formula](#4-formulas), which returns an Array of all option-keys, e.g. from `lookup()`. These can also be comma-separated values (CSV).
- **Formula for Label-Options** (optional): The [formula](#4-formulas), which returns an Array of all option-labels, e.g. from `lookup()`. These can also be comma-separated values (CSV).
- **Default value** (optional): If set, the drop-down list will select the `key` entered as default value. If not set, the drop-down list will include an empty option at the beginning of the list.

### 3.8. Panel

Panels are invisible containers designed to display other components in a specific layout.

![Panel configuration dialog](docs/imgs/new-panel.png)

- **Key**: Optional
- **Panel layout**: Sets the future components layout in the panel
  - **Vertical**: Components will display vertically, one after the other, taking all the available width.
  - **Horizontal**: Components will display horizontally, from left to right.
  - **Grid**: Components will display horizontally, in lines of n components.
- **Panel alignment**: Sets the alignment of the components
  - **Center**: Will center all components in their virtual column
  - **Left**: Will place components on the left, leaving a space on the right of the panel
  - **Right**: Will place components on the right, leaving a space on the left of the panel
  - **Justify**: Will center components in their columns, but will not leave space in the left and right of the panel.
- **Is Panel collapsible?**: Should the panel be collapsible in the sheet?
- **Is Panel collapsed by default?**: Should the panel be collapsed in the sheet on the first display?
- **Panel title**: The Panel title to toggle collapsed state
- **Panel title style**: The style of the Panel title

Once created, the panel will look like this in the template editor:

![Panel editor](docs/imgs/panel-editor.png)

The `+` button is used to add new components to the panel, in the same way as the body and header. The panel's header indicates the panel's key for reference, and will not be shown in the definitive sheets, nor will be the outline. The panel's header is used to edit the panel's settings.

### 3.9. Tabbed Panel

Tabbed Panels are containers designed to display other components in predefined tabs.

![Panel configuration dialog](docs/imgs/new-tabbed-panel.png)

- **Key**: Optional

Once created, the panel will look like this in the template editor:

![Panel editor](docs/imgs/tabbed-panel-editor.png)

The panel's header indicates the panel's key for reference. Neither will it be shown in the definitive sheets, nor will be the outline. The panel's header is used to edit the panel's settings.

To add a tab to your sheet, click the `+` icon in the tab selector section. This opens a pop-up dialog to let you enter the tab details.

![Add new tab](docs/imgs/new-tab.png)

You must enter the tab name, which can be anything, and the tab key.

You can edit the currently selected tab name and key with the edit button on the right, as well as delete it. Additionally, you can reorder your tabs using the arrows on the left and right of the tab name.

![Tab controls](docs/imgs/tab-controls.png)

### 3.10. Table

Tables are strict containers with specified column and row counts.

![Table configuration dialog](docs/imgs/new-table.png)

- **Key**: Optional
- **Row count**: How many rows should the table have?
- **Column count**: How many columns should the table have?
- **Table layout**: This is a code to specify the columns' alignment. Each column has 1 letter in the code, which indicates if the column should be aligned to the left (`l`), to the right (`r`) or centered (`c`).

Once created, the table will look like this in the template editor :

![Table editor](docs/imgs/table-editor.png)

The `+`-buttons are used to add new components to the tables' cells, in the same way as the tabs and header. The tables' header indicates the tables' key for reference, and will not be shown in the definitive sheets, nor will be the outline. The tables' header is used to edit the tables' settings.

### 3.11. Dynamic table

The Dynamic table is a component which allows players to make custom lists in their sheets with a layout specification.

![Dynamic table configuration dialog](docs/imgs/new-dtable.png)

- **Key**: Mandatory - will not save otherwise
- **Make column name bold**: If set, column names will be bold on the table, to emphasize the head of the table.
- **Show confirmation dialog on row delete**: If set, a confirmation dialog will be shown on click on the delete row button, to avoid accidental row deletion
- **Can players add rows in the table**? : If set, players will be able to add rows in their sheets. If not, only GMs & Assistant GMs will be able to.
- **Sort entries**
  - **Sort options**: You can select 3 different sort-options: `Auto`, `Manual` and `Disabled`.
    - **Auto**: This option allows you to sort entries automatically whenever the character sheet gets updated
    - **Manual**: With this option on, each entry will display arrows on the right side of the Table. With this, players can sort their entries however they like. Players can also click a column header to sort the table by this column.
    - **Disabled**: This option disables any sorting-behavior, so entries are displayed in the order they are added to the character sheet
  - **Add sorting criteria**: This will only be shown when `Auto` is selected. In here you can configure the sorting-behavior. You can add multiple criteria for sorting, but the sort-priority is determined by insertion order:
    - **Column key**: The column-key you want to sort after
    - **Operator**: The Comparison-Operator, which should be used for Sorting. `ASC` and `DESC` will sort the entries by natural order (`ASC`: 0-9, a-z; `DESC`: 9-0, z-a), while `==` and `!=` will prioritize those entries, which match the criteria
    - **Value**: The value to compare to. When using `ASC` or `DESC`, this is irrelevant.

Once created, the dynamic table will look like this in the template editor:

![Dynamic table editor](docs/imgs/dynamic-table-editor.png)

The `+` button in the header row is used to create new columns to the dynamic table. The dialog that opens is the same as panels, tabs and headers components, with two additional configuration settings:

![Dynamic table - Add new column](docs/imgs/dynamic-table-new-component.png)

- **Alignment**: How the column should be aligned in the dynamic table
- **Column name**: If set, will be displayed as the column's name

The `+`-button in the body of the table is used to create predefined lines. These lines will get added to the characters synchronized with the template. Each field can be filled with predefined data, which will be editable later by the players.

Each line will only be added once to every player. If a player were to delete the corresponding line in their sheet, there would be no built-in way to recover the line. Lines added after a character creation will however be added to the character sheet upon template reloading.

Each predefined line can be made permanent on the character sheet by activating the slashed trash icon at the end of the line. This setting will update existing predefined lines in character sheets upon template reloading.

The fields are not validated in the templates, it is up to the designer to make sure the data typed in the field is valid with the field's rules.

### 3.12. Item container

Item containers are components designed to display the items applied to an actor. Items can be filtered in the container to ensure sheet coherence, and specific information can be added next to the items.

![Item container configuration dialog](docs/imgs/new-item-container.png)

- **Key**: Mandatory
- **Table Title**: A title to display on the top of the item container. This can contain HTML.
- **Item reference column label**: Every item container displays at least one column, which is a reference to the item. This setting allows you to configure its label.
- **Item reference column alignment**: This setting allows you to configure the reference column alignment.
- **Hide empty**: If set, The Item Container will be hidden if it displays no items.
- **Display column names**: If set, column names will be displayed on the table. Can be unset to refine the items display.
- **Make column name bold**: If set, column names will be bold on the table, to emphasize the head of the table.
- **Show item delete button**: If set, each item will be accompanied by a delete button, allowing players to delete their own items.
- **Show confirmation dialog on item delete**: If set, a confirmation dialog will be shown on click on the delete row button, to avoid accidental item deletion.
- **(GM only) Show status icon**: If set, GMs will see a status icon next to the item name, indicating if the item's template exists in the world. If the template exists, clicking this icon will open the template sheet.
- **Sort items**
  - **Sort options**: You can select 3 different sort-options: `Auto`, `Manual` and `Disabled`.
    - **Auto**: This option allows you to sort Items automatically whenever the character sheet gets updated
    - **Manual**: With this option on, each Item will display arrows on the right side of the Container. With this, players can sort their Items however they like. Players can also click a column header to sort the table by this column.
    - **Disabled**: This option disables any sorting-behavior, so Items are displayed in the order they are added to the character sheet
  - **Add sorting criteria**: This will only be shown when `Auto` is selected. In here you can configure the sorting-behavior. You can add multiple criteria for sorting, but the sort-priority is determined by insertion order:
    - **Field key**: The key of an Item property (without the `item.`-prefix)
    - **Operator**: The Comparison-Operator, which should be used for Sorting. `ASC` and `DESC` will sort the Items by natural order (`ASC`: 0-9, a-z; `DESC`: 9-0, z-a), while `==` and `!=` will prioritize those Items, which match the criteria
    - **Value**: The value to compare to. When using `ASC` or `DESC`, this is irrelevant.
- **Filter items**
  - **Select templates**: You can select which item templates are eligible to be displayed in this container. This is a first filter for the container. If no templates are checked, no filter will be done on templates, only custom filters will apply.
  - **Item Filter Formula**: You can define additional filters by adding a Formula (without `${}$`). The Item Container will only display those Items, for which the Formula resolves to `true`. You can access item-properties with `item.<key>` (similar to Labels in Item Containers).

Once created, the item container will look like this in the template editor :

![Item container editor](docs/imgs/item-container-editor.png)

The `+`-button is used to create new columns to the item container. The dialog that opens is the same as dynamic table components, but with only Labels available. You can only configure labels in item containers, no value can be entered in this component.

As with any labels, you can use formulas in the Label text and Roll Messages. The keys used in the formulas will reference the parent keys, but you can use the item keys by adding `item.` in front of the key. For example, if a weapon's damage value is based on the character's `STRMod` and an internal `damage` value, you can display it in the item container with the formula `${STRMod + item.damage}$`.

Lastly: If no additional columns are set and `Display column names` is unchecked in the Item container configuration, the items will be displayed as a vertical list without the table formatting.

### 3.13. Conditional Modifier List

The Conditional Modifier List is a component, which can be used to list and select conditional modifier groups. Modifier groups can be created/edited in the modifier configuration of the corresponding items (see [Item modifiers](#52-item-modifiers)). If you select a group, all modifiers from this group will be applied to the actor.

![Conditional Modifier List](docs/imgs/new-conditional-modifier-list.png)

- **Key**: Optional
- **Display column names**: If set, column names will be displayed on the table.
- **Display info icon**: If set, will display an info icon, which will display the description of all modifiers of the group when hovering over with the mouse.
- **Make column names bold**: If set, column names will be bold on the table, to emphasize the head of the table.
- **Filter modifier groups**: You can filter modifier groups by selecting them. If you select none, filtering will not be applied, thus it will display all conditional modifiers
- **Selection column name**: The label name of the selection column.
- **Selection column alignment**: This setting allows you to configure the selection column alignment.
- **Group column name**: The label name of the group column.
- **Group column alignment**: This setting allows you to configure the group column alignment.
- **Advanced - Group filter formula**: You can filter modifier groups by having a Formula computing the groups that should be displayed. The formula should output the groups names, separated by commas `,`. It should not be enclosed in `${}$`. If a formula is set, the group selection made by checkboxes will not apply, being overridden by the Formula.

> &#x26a0;&#xfe0f; **Issues with the "Combat-Utility-Belt"-Module**
>
> If you had this module installed, chances are high, that you´ll get errors when trying to open the config-form. To fix that issue, you should run the following script in the console (F12):
>
> `game.actors.get('<ID>').update({system: {activeEffects: {"-=combat-utility-belt": true}}});` where 'ID' is the ID of the Template (not Actor). Alternatively you can use `.getName('<TemplateName>')` instead of `.get('<ID>')`.

### 3.14. Meter

Meter are gauges displayed on the field, with configurable values and behaviour.

![Meter configuration dialog](docs/imgs/new-meter.png)

- **Key**: Mandatory - will not save otherwise
- **Label**: If set, will display this label next to the field on the sheet
- **Size**: Specifies the size of the field. 4 options are available :
  - **Auto**: The field will take all the available space, resizing automatically.
  - **Small / Medium / Large**: The field will have a fixed size, depending on your choice.

**All fields below can contain formulas, and should resolve to a number when computed**

- **Value**: The value displayed by the gauge. If the gauge has the key, this value is referencable in other Formulas.
- **Min**: The minimum value accepted by the gauge. If not set, defaults to 0.
- **Max**: The maximum value accepted by the gauge. If not set, defaults to 1.
- **Low**: An arbitrary low value in the gauge. If set without an optimum value, a value below this value will be displayed in yellow. Should be below the "High" value.
- **High**: An arbitrary high value in the gauge. If set without an optimum value, a value above this value will be displayed in yellow. Should be above the "Low" value.
- **Optimum**: An arbitrary optimum value in the gauge. This is only useful with High and Low values.
  - If set below the Low value, any value below the low value will be displayed in green, any value between Low and High will be displayed in yellow, and any value above High will be displayed in red.
  - If set above the High value, any value below the Low value will be displayed in red, any value between Low and High will be displayed in yellow, and any value above High will be displayed in green.
  - If set between the Low and High value, any value below the Low value will be displayed in yellow, any value between Low and High will be displayed in green, and any value above High will be displayed in yellow.
- **Text display**: Choose if and how the value should be displayed on the meter :
  - None : Will not diplay the value, and the meter will show slightly thinner.
  - Show value : Will show the raw value of the meter
  - Show value and maximum : Will show the value and the maximum formatted like this : `value / maximum`
  - Show value as percentage : Will calculate the percentage of the value between the minimum and the maximum values, and display it.

## 4. Formulas

In Label text or Roll Messages, you can use Formulas to enhance your sheets.

### 4.1. Plain formulas

To mark a formula inside your text, use the delimiters `${` and `}$`. Inside those delimiters, you can use the keys you defined for any label, text field, rich text area or checkbox. You can also use mathematical operators, as well as common mathematical functions, such as `floor()`, `ceil()` or `abs()`. Formulas are parsed with [the math.js library](https://mathjs.org), so you can use any expression valid in this library. Here is [a summary of the syntax](https://mathjs.org/docs/expressions/syntax.html) and [a summary of the available functions](https://mathjs.org/docs/reference/functions.html).

For example, the modifier formula for Strength in DnD can be expressed as `${floor((STR - 10) / 2)}$`, assuming the Strength input was given the key `STR`.

### 4.2. Additional functions

In addition to the math.js library, a few specific functions are available in the system.

> &#x26a0;&#xfe0f; **When to use quoted strings and when to use direct field key ?**
>
> In the following functions, you'll see use of quoted `''` field keys and unquoted ones. The difference between the two forms is that unquoted field keys will be replaced by their value at runtime. For example, for an `hp` field with value `10`, `function(hp)` would be equivalent to `function('10')`. For the `ref` function for example, this is a problem : `ref(attribute)` aims at getting the value of the field referenced by the `attribute` field. If `attribute`'s value is `str`, `ref(attribute)` is equivalent to `ref('str')`, and will return the value of the field `str`. And `ref('attribute')` will return the string `str`.
>
> Examples of this are given in the function's description, please read them carefully!

#### 4.2.1. lookup

`lookup(extensibleTableKey : String, targetColumn : String, [filterColumn : String], [filterValue : String | Number | boolean], [comparisonOperator : String]) : Array<T>`

This function is used to retrieve values from a Dynamic Table or an Item Container. Values can be filtered by a different column and different operations.

**Arguments**

- **extensibleTableKey** : String

The key to the Dynamic Table or Item Container to get the value from

- **targetColumn** : String

The column key from which to get the values. In Item Containers, the keys `id` and `name` refer respectively to the id and the name of the item.

- (optional) **filterColumn** : String

An optional column key to filter the results. In Item Containers, the keys `id` and `name` refer respectively to the id and the name of the item.

- (optional) **filterValue** : String | Number | boolean

The value to filter the results. If not provided, nothing will be returned.

- (optional) **comparisonOperator** : String

Compares each `filterColumn` with the `filterValue` by this operator. Default comparison is `===`. You can use every comparisonOperator described in [Conditions](#43-conditions) together with `===`, `!==` and `~`.

The `~` operator performs a RegExp matching with the filterValue.

**Returns**
An Array containing all the column values where filter is met.

**Examples**

- Fetch all values

Suppose you have the following Dynamic Table, whose key is `equipment`:

| Name (key `name`) | Weight (key `weight`) | Location (key `location`) |
| ----------------- | --------------------- | ------------------------- |
| Sword             | 50                    | Backpack                  |
| Deck of cards     | 2                     | Pocket                    |
| Shield            | 30                    | Backpack                  |

`lookup('equipment', 'weight')` : Will return an Array containing all the weights from the Dynamic Table equipment, here: `[50, 2, 30]`

- Fetch values with filter

With the same table, you can add a filter to get only the Backpack items.

`lookup('equipment', 'weight', 'location', 'Backpack')` : Will return an Array containing all the weights from the Dynamic Table equipment, where `location` is equal to `Backpack`, here: `[50, 30]`

- Fetch values with filter (and comparisonOperator)

With also the same table, you can filter by a specific weight threshold (here: at least 10).

`lookup('equipment', 'weight', 'weight', 10, '>=')` : Will return an Array containing all the weights from the Dynamic Table equipment, where `weight` is greater-equals than 10, here: `[50, 30]`

- Filter values with another field

Suppose you have the following Dynamic Table, whose key is `skills`:

| Name (key `name`) | Level (key `skill_level`) |
| ----------------- | ------------------------- |
| Craft             | 5                         |
| Lock-pick         | 2                         |
| Hide              | 7                         |
| Craft             | 7                         |

`lookup('skills', 'value', 'name', selectSkill)` : Will return an Array containing all the values from the Dynamic Table skills, for the rows which name is equal to the value of the field `selectSKill` (Notice `selectSkill` does not have quotes around it, meaning it is a field in the sheet).

If `selectSkill`'s value is 'Lock-pick', this will return `[2]`.

If `selectSkill`'s value is 'Craft', this will return `[5, 7]`.

#### 4.2.2. lookupRef

`lookupRef(extensibleTableKey : String, targetColumn : String, filterColumn : String, filterValue : String | Number | boolean) : String`

This function is used to retrieve the data-path of a field in a Dynamic Table or an Item Container. The correct row is filtered using a column key and a value, which must be unique across the Dynamic Table / Item Container.

**Arguments**

- **extensibleTableKey** : String

The key to the Dynamic Table or Item Container to get the value from

- **targetColumn** : String

The column key from which to get the values. In Item Containers, the keys `id` and `name` refer respectively to the id and the name of the item.

- **filterColumn** : String

The column key to filter the results. In Item Containers, the keys `id` and `name` refer respectively to the id and the name of the item.

- **filterValue** : String | Number | boolean

The value to filter the results.

**Returns**
A String corresponding to the current data-path of the field in the system, usable for editing its value with `setPropertyInEntity` or modifiers.

**Examples**

Suppose you have the following Dynamic Table, whose key is `equipment` :

| Name (key `name`) | Weight (key `weight`) | Location (key `location`) |
| ----------------- | --------------------- | ------------------------- |
| Sword             | 50                    | Backpack                  |
| Deck of cards     | 2                     | Pocket                    |
| Shield            | 30                    | Backpack                  |

`lookupRef('equipment', 'weight', ',name', 'Shield')` : Will return the data path to the weight property of the `Shield` row.

#### 4.2.3. first

`first(list : Array<T> | Matrix<T>, [fallbackValue : *]) : T`

Returns the first value of an Array or a Matrix.

**Arguments**

- **list** : Array<T> | Matrix<T>

An Array or a Matrix containing elements of type T (usually String, Number or boolean).

- (optional) **fallbackValue** : \*

This value will be returned if list is empty or null.

**Returns** The first value of list, or fallbackValue if list is empty or null.

**Examples**

With the `skills` Dynamic table defined in the previous part.

`first(lookup('skills', 'value', 'name', 'Craft'), 0)` : Will get the value of the first Craft skill if the skills Dynamic Table, or 0 if Craft is not in the table. Here, this will return `5`.

You will need to use `first()` to get values as Numbers, even if the Dynamic Table's filter returns only one row. The function `lookup()` returns an Array, which is not suited for most operations.

#### 4.2.4. sameRow

`sameRow(columnName: String, [fallbackValue: *]) : String | Number | boolean`

Returns the value of the column with the key `columnName` in the current row in a Dynamic Table.

**Arguments**

- **columnName** : String

The key of the column to target.

- (optional) **fallbackValue** : \*

The value to return if the column is not found or does not contain data.

**Returns** the value of the column with the key `columnName` in the current row in a Dynamic Table, or `fallbackValue` if the column does not exist or does not contain data.

**Examples**

Suppose you have the following Dynamic Table, whose key is `skills` :

| Name (key `name`) | Base (key `base_value`) | Bonus (key `bonus_value`) |
| ----------------- | ----------------------- | ------------------------- |
| Craft             | 5                       | 3                         |
| Lock-pick         | 2                       | 4                         |
| Hide              | 7                       | 2                         |
| Craft             | 7                       | 3                         |

If your skill level is the `base_value` + the `bonus_value`, you can automate this calculation by using `sameRow('base_value', 0) + sameRow('bonus_value', 0)`. These functions will get the value of respectively `base_value` and `bonus_value` in the same row as the Formula being computed, or `0` if the value is undefined. This would get you the following Dynamic Table.

| Name (key `name`) | Base (key `base_value`) | Bonus (key `bonus_value`) | Level (key `skill_level`)                                          |
| ----------------- | ----------------------- | ------------------------- | ------------------------------------------------------------------ |
| Craft             | 5                       | 3                         | `${sameRow('base_value', 0) + sameRow('bonus_value', 0)}$` => `8`  |
| Lock-pick         | 2                       |                           | `${sameRow('base_value', 0) + sameRow('bonus_value', 0)}$` => `2`  |
| Hide              |                         | 5                         | `${sameRow('base_value', 0) + sameRow('bonus_value', 0)}$` => `5`  |
| Craft             | 7                       | 3                         | `${sameRow('base_value', 0) + sameRow('bonus_value', 0)}$` => `10` |

#### 4.2.5. sameRowRef

`sameRowRef(columnName: String) : String`

Returns the data-path of the column with the key `columnName` in the current row in a Dynamic Table.

**Arguments**

- **columnName** : String

The key of the column to target.

**Returns** the data-path of the column with the key `columnName` in the current row in a Dynamic Table, usable for editing its value with `setPropertyInEntity` or modifiers.

#### 4.2.6. sameRowIndex

`sameRowIndex(): Number`

**Returns** the index of the current row in a Dynamic Table. Usable if you want to have a row-count.

#### 4.2.7. ref

`ref(valueRef : String, [fallbackValue : *]) : *`

Returns the value of the field with the key in valueRef.

**Arguments**

- **valueRef** : String

The key of the field to get the value from.

- (optional) **fallbackValue** : \*

The value to return if the key referenced has no value.

**Returns** the value of the field which has the key pointed by valueRef, or fallbackValue if valueRef does not point to a valid field.

**Examples**

`ref('hp')` Returns the value of the field with the key `hp`

`ref(currentArmor)` Returns the value of the field targeted by `currentArmor`. If `currentArmor` is a field containing `lightArmorProtection` for example, this will return the value of the `lightArmorProtection` field.

`ref(sameRow('attribute'))` Returns the value of the field targeted by the column `attribute` in the current row of a Dynamic Table.

#### 4.2.8. replace

`replace(text : String, pattern : String, replacement : String) : String`

The `replace()` function replaces the first occurrence of the pattern in the text with the replacement.

**Arguments**

- **text** : String

The text you want to modify. It can also be a key to a field.

- **pattern** : String

The pattern which will be replaced.

- **replacement** : String

The replacement text, which will replace the pattern.

**Returns** the new text with the replacement.

**Examples**

`replace('My test test', 'test', 'dog')` Replaces the first occurrence of 'test' with 'dog'. Output: `My dog test`.

`replace(MyInputText, 'random', '')` With 'MyInputText' = `This is a random sentence. Very random!` => Returns: `This is a  sentence. Very random!`

#### 4.2.9. replaceAll

`replaceAll(text : String, pattern : String, replacement : String) : String`

The `replaceAll()` function replaces all occurrences of the pattern in the text with the replacement.

**Arguments**

- **text** : String

The text you want to modify. It can also be a key to a field.

- **pattern** : String

The pattern which will be replaced.

- **replacement** : String

The replacement text, which will replace the pattern.

**Returns** the new text with the replacement.

**Examples**

`replaceAll('My test test', 'test', 'dog')` Replaces all occurrences of 'test' with 'dog'. Output: `My dog dog`.

`replaceAll(MyInputText, 'random', '')` With 'MyInputText' = `This is a random sentence. Very random!` => Returns: `This is a sentence. Very !`

#### 4.2.10. recalculate

`recalculate(formula : String) : String | Number | boolean`

This function is used to recalculate a value. This can be used for custom formulas from a Text Field or a Rich Text Area. **BEWARE: It cannot recalculate Roll-Formulas or User Input Dialogs**

**Arguments**

- **formula** : String

The formula which should be recalculated. It needs to contain `${}$` as usual. Can be a key to a field with a formula.

**Returns** the result of the calculation.

**Examples**

`recalculate('${Strength + 3}$')` With 'Strength' = `7` => Returns: `10`

`recalculate(MyInputFieldKey)` With 'MyInputFieldKey' = `${round(Strength / 2)}$` and 'Strength' = `5` => Returns: `3`

#### 4.2.11. fetchFromActor

`fetchFromActor(actorName : String, formula : String, [fallbackValue : *]) : String | Number | boolean`

**Arguments**

- **actorName** : String

The name of the actor to reference. It can also contain special values like:

- `selected`: Gets the actor-data of the currently selected token
- `target`: Gets the actor-data of the currently targeted token
- `attached` (Item only): Gets the actor-data an Item is attached to

- **formula** : String

The formula which should be calculated on the referenced actor. It can also just be a key.

> &#x26a0;&#xfe0f; **Be careful with string-delimiters**: Use `'` instead of `"` inside the formula if you want to use quotes inside quotes. `"` will be ignored by the target-actor. See the last example below if you want to see how it should be used.

- (optional) **fallbackValue** : \*

The value to return if the referenced actor couldn´t be found or returns no value.

**Returns** the value of the field which has the key pointed by valueRef, or fallbackValue if valueRef does not point to a valid field.

**Examples**

- Get value of key `Defense` from the current target: `fetchFromActor('target', "Defense")`
- Get value of key `Strength` from the actor `John`: `fetchFromActor('John', "Strength")`
- Get value of key `Intelligence` from the actor `Max`. Return `0` if actor or property is not present: `fetchFromActor('Max', "Intelligence", 0)`
- Get value of key `Skill_Value` from dyamic table `Skills` where `Skill_Name` equals `Lock Picking`from the current target: `fetchFromActor('target', "lookup('Skills', 'Skill_Value', 'Skill_Name', 'Lock Picking')")`

#### 4.2.12. setPropertyInEntity

`setPropertyInEntity(entityName : String, propertyName : String, formula : String, [fallbackValue : *]) : String | Number | boolean`

**Arguments**

- **entityName** : String

The name of an actor to reference. It can also contain special values like:

- `self`: Sets into the current actor (can be used in labels, initiative formula and attribute bars)
- `selected`: Sets into the actor-data of the currently selected token
- `target`: Sets into the actor-data of the currently targeted token
- `attached` (Item only): Sets into the actor-data an Item is attached to
- `item` (Actor only, inside Item Containers): Sets into the item-data of the current row of the item container

Items cannot be referenced by their name.

- **propertyName** : String

The name of the property to set

- **formula** : String

The formula which should be calculated on the referenced actor or item. It can also just be a key. Inside Item Containers, item properties should be targeted with the `item.` prefix, just like in other parts of the formulas. If you need to use the properties of the target-actor, you can achieve that by using the `target.`-prefix.

> &#x26a0;&#xfe0f; **Be careful with string-delimiters**: Use `'` instead of `"` inside the formula if you want to use quotes inside quotes. `"` will be ignored by the target-actor. See the last example below if you want to see how it should be used.

- (optional) **fallbackValue** : \*

The value to return if the formula returns no value.

**Returns** the value of the formula.

**Examples**

- Set value of key `HP` to 20 in the current actor: `setPropertyInEntity('self', 'HP', 20)`
- Double value of key `Defense` in the current target: `setPropertyInEntity('target', 'Defense', "target.Defense * 2")`
- Decrease the value of the key `Health` (target) by the value of the key `Damage` (self) in the current target: `setPropertyInEntity('target', 'Health', "target.Health - Damage")`
- Set value of key `Strength` to 5 from the actor `John`: `setPropertyInEntity('John', 'Strength', 5)`
- Decrease the ammunition total by one in the current item in an Item Container : `setPropertyInEntity('item', 'Ammunition', "item.Ammunition - 1")`
- Set value of key `Current_Armor_Value` to the Plate Armor value: `setPropertyInEntity('self', 'Current_Armor_Value', "lookup('Armors', 'Armor_Value', 'Armor_Name', 'Plate Armor')")`
- Decrease value of the field `Uses_left` in the `Spells` Dynamic Table for the spell named `Fireball` : `setPropertyInEntity('self', lookupRef('Spells', 'Uses_left', 'Name', 'Fireball'), "min(first(lookup('Spells', 'Uses_left', 'Name', ireball')) - 1, 0)")`
- Reset value of the field `Charges` row of a Dynamic Table : `setPropertyInEntity('self', sameRowRef('Charges'), "sameRow('MaxCharges')")`

#### 4.2.13. consoleLog and consoleTable

`consoleLog(dataLog : *)`

Renders a console log of the string or object passed to it.

`consoleTable(dataLog : *)`

Renders a console table of the string or object passed to it.

These functions are intended to be used for debugging during sheet building, and should likely not be used in your final formulas.

**Examples**
In an item container, you can inspect the properties of an item like so: `${consoleTable(item)}$`

**Arguments**

- **dataLog** : \*

The data to log to the console

#### 4.2.14. switchCase

`switchCase(expression: *, case1: *, value1: *, [case2: *, value2: *, ...], [defaultValue: *]): *`

Tests an expression against a list of cases and returns the corresponding value of the first matching case, with an optional default value if nothing else is met. The behaviour is identical to the `SWITCH()`-function in Excel or Google Sheets.

**Arguments**

- **expression** : \*

Any valid values.

- **case1** : \*

The first case to be checked against expression.

- **value1** : \*

The corresponding value to be returned if case1 matches expression.

- (optional) **case2, value2, ...** : \*

Additional cases and values if the first one doesn’t match the expression.

- (optional) **defaultValue** : \*

An optional value, specified as the last parameter, to be returned if none of the cases match the expression.

**Returns** the value matching the corresponding case. Otherwise, returns the default value.

**Examples**

- `switchCase(DropdownKey, 'a', 1)` Returns `1` if DropdownKey is `a`. Otherwise, it returns null.
- `switchCase(DropdownKey, 'a', 1, 'b', 2, 'c', 3, 0)` Returns `1` for `a`, `2` for `b`, `3` for `c`. If none of the cases match, it returns `0` as default value.

#### 4.2.15. notify

`notify(messageType: String, message: String): String`

Sends a notification to the user.

**Arguments**

- **messageType** : String

The message type of the notification. Valid types are `info`, `warn` and `error`.

- **message** : String

The message which should be displayed in the notification.

**Returns** the message

**Examples**

- `notify('info', 'You have spend 1 Action-Point')` Sends an info-notification with the message `You have spend 1 Action-Point`.
- `notify('warn', 'Target not selected')` Sends a warn-notification with the message `Target not selected`.
- `notify('error', 'No points left')` Sends an error-notification with the message `No points left`.

#### 4.2.16. escapeQuotes

`escapeQuotes(value: String): String`

Escapes simple quotes in a string value.

**Arguments**

- **value** : String

The value to escape.

**Returns** the escaped value

**Examples**

- `escapeQuotes("Test message")` outputs `Test message`.
- `escapeQuotes("Test message with ' a quote")` outputs `Test message with \' a quote`.

### 4.3. Conditions

You can add conditions to your formulas using the ternary operator : `<condition> ? <value if true> : <value if false>`.

The condition part is evaluated as a boolean :

- You can use comparison operators like
  - `==` for equality
  - `>` and `>=`, respectively 'greater than' and 'greater than or equals'
  - `<` and `<=`, respectively 'lower than' and 'lower than or equals'
  - `!=` for 'not equals'
- Any number other than 0 will be considered true
- You can directly use the key of a checkbox to test it is checked
  - Example : `${ checkboxKey ? 'Checked' : 'Not checked' }$`

> &#x26a0;&#xfe0f; **Some common syntax in other programming languages won't work**: For example, you can not use the 'NOT' `!` operator.

### 4.4. Use text

You can add text to your formulas to add flavor to your sheets and messages. To use text, you must enclose it in quotes `'`. If you wish to use a quote in the text, you can prefix it by a backslash `\`, it will be parsed correctly. For example : `${'You can\'t use this item !'}$`

You can concatenate predefined text and field values by using the function `concat()`. If you do so, field values must be enclosed in the `string()` function to work correctly. For example : `concat('Deal ', string(strMod+dmgMod), ' damage to the target.')`.

### 4.5. Scripts

You can include JavaScript if you need values from outside the system. You can mark these Scripts with `%{}%`. For example: `%{return game.system.version;}%` will return the version number of the system.

The system provides the context of the triggering entity (actor or item), linked entity (Item in Item Container), the local Variables of Label Roll Messages and some additional context-dependent options. You can access them by using the `entity`/`linkedEntity`/`localVars`/`options`-objects (see example below)

> &#x26a0;&#xfe0f; It is important that you use `return` to actually return the value. Otherwise, it will return `undefined`.

You can include Scripts into Formulas and vice versa, so it´s possible to transfer values between Scripts and Formulas. Here are some examples:

- `%{return game.actors.size + ${Strength}$;}%`: Will add the amount of all actors in the game with the Strength
- `${%{return game.actors.size}% + Strength}$`: Basically does the same
- `%{return await game.macros.getName('YourMacro').execute()}%`: Will execute the macro with the name `YourMacro` and return its result
- `%{return await game.macros.getName('YourMacro').execute({name: '${name}$'})}%`: Will execute the macro with the name`YourMacro`, pass `name` as an argument to the macro and return its result
- `%{return entity.entityType}%` Will return the type of the triggering entity (either `actor` or `item`)
- `%{return entity.system.props.Health}%` Will return the value of `Health` in the triggering entity
- `%{linkedEntity.delete()}%` This will delete the Item in the Item Container, if the script is executed within the Label Roll Message of the Item Container. **ATTENTION: Use delete-operations with care!**
- `%{return localVars.roll}%` This will return the value of `roll` in the local variables of a Label Roll Message
- `%{localVars.something = 5;}%` This will add `something` with the value `5` to the local variables of the Label Roll Message, which can be used by other Formulas further down.
- `%{return options.dynamicRowRef}%` This will return the row-reference of the Dynamic Table the Script was executed in. This can be helpful in Scripts, where the reference of the current row is needed.

### 4.6. Add rolls

> &#x2139;&#xfe0f; The roll system is compatible with the Dice so Nice module!

You can use rolls in your formulas. To do so, inside the formula markers, use brackets (`[` and `]`) to define your rolls. In roll expressions, you can also use keys, but you must surround them with colons. Rolls are performed using FoundryVTT Roll API.

For example, to roll for Shadowrun's Strength, you can define the following formula : `${[:STR:d6cs>=5]}$`.

Roll operations can be defined inside the roll, or outside. In terms of pure result, `${[1d20 + 3]}$` and `${[1d20] + 3}$` are equivalent, but I would recommend using the latter.

You can also use Roll Tables, using the syntax `[#<rollTableName>]`. Just put a `#` before your roll table name, inside brackets, and the system will roll its default formula and integrate the result in the message.

You can customize the Roll Table formula by adding a Roll formula in this syntax : `[#<rollTableName>|<rollFormula>]`. The formula must follow roll formula syntax, with `::` to delimit the keys. This formula can be a Dice roll or a constant value, the latter will always draw the same result from the Roll Table.

When used in chat messages, formulas are explained by a small box displayed on click.

![Chat example](docs/imgs/chat-example.png)

> &#x26a0;&#xfe0f; **Rolls can be used only in messages, not in label values.**

### 4.7. Reuse formula results

In case you want to re-use formula results in a chat message, to display additional information, you can assign a formula result to a variable using the operator `:=`. You can then reuse the variable as a key later on in the same message.

For example, using Warhammer's SL system :

```
Rolling ${roll:=[1d100]}$.
SL = ${SL:=floor(WS/10) - floor(roll/10)}$.
Damage : ${9 + SL}$
```

### 4.8. User inputs

Sometimes you will need to ask your players for input when computing a Label message. You have two ways of doing so : You can use simple prompts, or craft a more complex User Input Template.

#### 4.8.1. Simple prompts

**Syntax**

```
?{<varName>[:<Display Name>][varType][|<defaultValue>]}
?{<varName>[:<varType>][|<choice1>[:<display 1>]|<choice2>[:<display 2>]|<choice3>[:<display 3>]|...]}
```

You can ask for user inputs in formula using the syntax `?{<varName>}`. This will open a pop-up dialog during formula computing asking for all the inputs in the formula.

You can specify an alternative name in the dialog by adding a colon and typing your sentence. This display name is a formula without the enclosing `${}$`. If you wish to just set a character string, it should be enclosed in quotes `'`.

You can add a default value by adding it after a vertical bar, like so : `?{<varName>|<defaultValue>}`. The `defaultValue` is a formula, without the enclosing `${}$`. If `defaultValue` is a character string, it should be enclosed by quotes `'`.

You can set the input to be a choice by adding more options after the vertical bar. In that case, the input will be a dropdown list, with all your choices. The first choice will be selected by default. Each choice can have a value and a displayed text, by adding a colon and a formula after the value in the choice. The value and the displayed text are formulas without the enclosing `${}$`. If you wish to just set a character string, it should be enclosed in quotes `'`.

You can also set a type for the field by specifying it between brackets after the input name and the displayed name. The type can be `text`, `number` or `check`. `text` is the default.

`text` and `check` types support multiple choices, resulting in respectively a dropdown list and radio buttons. `number` is displayed as a text field, in which only numbers can be typed, and scrolling adds or substracts to its value.

The user inputs variables can be reused later on as keys in the same message.

> &#x26a0;&#xfe0f; \*_User inputs can be used only in messages, not in label values._

**Examples**

```
${#concat(
?{name:"Character name"[text]|"Your name"},
?{age[number]},
?{gender:"Character gender"[check]|"m","Male"|"f","Female"|"o","Other, or I don't want to say"},
?{month:'What month are you born ?'|"January"|"February"|"March"|"April"|"May"|"June"|"July"|"August"|"September"|"October"|"November"|"December"},
?{happy:"Are you happy ?"[check]}
)}$
```

Results in :

![User inputs](docs/imgs/user-inputs.png)

#### 4.8.2. User Input Templates

You can create a special kind of items called `userInputTemplates` to create your own custom input dialogs. You can add many components in the template, which will be rendered like an actor or item sheet when called upon, with a validation button added in the bottom.

To trigger it in a Label Roll Message, you can use the syntax `?#{<userInputTemplate name>}`. The template will be displayed using the actor or item data, if you want to use this data in Labels in the template. Any input field using a key already defined in the triggering actor or item will use its value as default value, but will not be saved in the actor or item.

### 4.9. Hide formulas in chat messages

When using formulas in chat-messages, you can hide some formulas to perform preliminary calculations assigned to reusable variables. To do so, prefix your formula with `#`, for example :

`${#testResult := target - roll}$`

> &#x26a0;&#xfe0f; **Formula hiding can be used only in messages, not in label values.**

### 4.10. Do not include explanation in chat message

When using formulas in chat-messages, you can use the Formula's result as is, without it being enclosed in the clickable box with the explanations. This can be useful if you want to call a macro with parameters calculated from Formulas. To do so, prefix your formula with `!`, for example :

`/myMacro ${!target - roll}$`

> &#x26a0;&#xfe0f; **Explanation hiding can be used only in messages, not in label values.**

## 5. Items

Okay, we now know how to build a basic character sheet, and how to augment it with automatic calculations and generated chat messages. Let's dig more and enter the wonderful world of Items!

An Item in FoundryVTT represents everything that is not a character for which you want to have a sheet. It can be actual items like weaponry or armours, or more abstract concepts like species or classes. Item can be attributed to actors or other items by dragging and dropping them onto other sheets. Caution: This attribution process creates a clone of said item, so any changes to an item's value won't be reported on attributed versions of the item, and vice versa.

### 5.1. Equippable items

To create items, you need to create a `_equippableItemTemplate` for the item. This works just like the actor's template creation, so I won't be detailing it any further.

Once the template is created, you can create an `equippableItem` and select its template just like you do for actors. The sheet renders, and you can fill the fields in.

On each `equippableItem` sheet, you will find a `Make item unique` checkbox. Checking it will prevent anyone from adding multiple instances of the same item to an actor sheet.

### 5.2. Item modifiers

On the `equippableItem` sheet, you can find a `Configure item modifiers` button. This button opens a window allowing you to create modifiers which will be applied to any actor this item is assigned to.

When creating a modifier, you must enter 6 configuration data :

1. **ConditionalGroup**: If specified, it will convert a modifier to a conditional modifier. Conditional modifiers will only be applied, if they are selected in a [Conditional Modifier List](#313-conditional-modifier-list). Modifiers can contain the same group. If a group gets selected, all modifiers from this group will be applied to the actor.
2. **Priority** : This indicates the order in which the modifiers will be applied, should multiple modifiers affect the same property. Lower values will be applied first. If multiple modifiers have the same priority, they are ordered by their operator : set are applied first, then multiplications, then divisions, then addition and finally subtraction. If multiple modifiers share the same priority and operation, their order is irrelevant as the mathematical result will be the same.
3. **Key** : This is the key of the property to modify in the owner actor. **This must be the key of a number containing Label (except for Set-operations).** Modifiers don't work on input-field-components like `number field` or `text field`. It can contain formulas, which will be calculated from the item´s data. To update a field in a Dynamic Table, you need to use a combination of Formula functions like this: `${fetchFromActor('attached', "lookupRef('<DYNAMIC TABLE KEY>', '<TARGET COLUMN KEY>', '<FILTER COLUMN>', '<FILTER VALUE>')")}$`
4. **Operator** : This indicates which operation the modifier should do : Addition (`+`), Subtraction (`-`), Multiplication (`x`), Division (`/`) or Set (`=`). The Set-operation sets the field to the modifier value with no other operation.
5. **Formula** : This is the value of the modifier. This can contain formulas, which will be calculated from the item´s data. This must be a **number** in the end for the computation to work. Text values can only be used with the Set-operation. There is also the possibility to not set a value if the formula result is '' (empty string), meaning that '' (empty string) would leave the referenced key untouched.
6. **Description** (optional): This is the description of the modifier, which will be displayed in the info-icon of the [Conditional Modifier List](#313-conditional-modifier-list), if the modifier has a group. You should provide one if your modifier is conditional, otherwise you can leave it empty. It can contain [Formulas](#4-formulas).

### 5.3. Item Template Modifiers

You can define modifiers on the `_equippableItemTemplate` as well. The only difference to `equippableItem`-modifiers is that these are applied to every Item created from this `_equippableItemTemplate`.

**For example:** If you want to sum up the `weight` of all the `Inventory`-Items an Actor has equipped, you´d define the modifier in the Item Template of `_Inventory` instead of on every `Inventory`-Item, because all your `Inventory`-Items would inherit the modifiers of the `_Inventory`-Template.

This leads to less modifier-redundancy and should be used wherever possible (weight/capacity from inventory, min-max-values from races, costs from buyable advantages, etc...).

### 5.4. Sub templates

There is another item type in Custom System Builder: the sub template. This item type can be used to create reusable template parts that can be added to other templates. The creation of this item is the same as any other templates, except it has no header. It also can not be used to create a definitive item.

When created, it can be dragged and dropped on any other template (actor template, equippable item template and sub templates). When dragging on a sheet, a grey outline will show you where the template can be dropped. Once dropped, each component defined on the subtemplate will be copied to the final template as is.

> &#x26a0;&#xfe0f; **This creates a copy of the elements. Any further modification on the subtemplate will not replicate to the definitive template. You will need to drop it again on the sheet to re-copy the components.**

## 6. Chat extensions

The Custom System Builder offers some convenient chat message extensions to write your own macros!

### 6.1. Roll Data and Token Data

Character properties are provided directly at the top of the RollData object. This means that you can use them directly with FoundryVTT roll system, like described [here](https://foundryvtt.com/article/dice-advanced/#data).

These properties are also available directly at the top of the TokenData object, meaning anything asking for a token variable can receive a property path.

### 6.2. Property reference inside a chat message

You can directly include a property inside a chat message by using the following syntax : `@{<actor>|<propertyKey>}`. Actor can be one of the following :

- `selected`, which will reference the selected token on the map or, if no character is selected, the default player's character.
- `target`, which will reference the targeted token on the map.
- Any character name, which will reference this character directly.

The referenced properties can be anything included in the roll data. You must use the property's key to reference it. You can additionally reference a specific row in dynamic table by using the following syntax : `@{<actor>|<dynamicTableKey>(<filterColumn>=<filterValue>).<targetColumn>`.

This will retrieve the value of `<targetColumn>` in the first row of the Dynamic Table where `<filterColumn>` is equal to `<filterValue>`.

For example, if you have a Skill table and want to retrieve the score of the `Lock picking` skill, you can use `skill(name=Lock picking).score`.

### 6.3. `/sheetRoll` and `/sheetAltRoll` commands

> &#x26a0;&#xfe0f; **This feature needs the module \_chatcommands installed and with version at least 1.4.0**

You can roll a predefined roll from a chat message with the `/sheetRoll` chat command. It is used with the key to the label containing the roll you want to trigger. For example : `/sheetRoll str` to trigger the roll associated with the `str` label.

To trigger dynamic table rolls, you must use a filter like the property reference, for example : `/sheetRoll skill(name=Lock picking).score` to trigger the roll associated with the label in the `score` column of the dynamic table `skill`, on the row which `name` is `Lock picking`.

The roll will be triggered for the currently selected character on the map, or the default player's character if no character is selected.

The `/sheetAltRoll` chat command does the same thing with an alternate chat message.

## 7. Macro support

### 7.1. Roll context menu

When right-clicking on a roll on a Character sheet, a context menu will appear, with options regarding macros. Three options are available :

1. Add as macro
   - This will show a dialog to create the new macro, asking for its name and place in the macro hotbar. When created, the macro configuration window will open for more configuration.
2. Copy chat command
   - This will copy the `/sheetRoll` command for this roll into your clipboard, allowing you to reuse it later.
3. Copy macro script
   - This will copy a basic script into your clipboard to use the roll in a script-type macro. This script is customizable to your needs.

This menu is only available on Labels which have keys.

### 7.2. Macro API

In a macro, you can use some of this system's features to enhance your game.

#### 7.2.1. `actor.roll`

With the `actor` object, which references the current actor, you can use the `roll` method to trigger a chat roll.

```
actor.roll(rollCode: string, [{ alternative = false: boolean, postMessage = true: boolean }]): ComputablePhrase
```

- `rollCode` is the code used with `/sheetRoll`
- `alternative`: if `true`, will roll the alternative roll message, if `false`, will roll the main roll message
- `postMessage`: if `true`, will automatically post the message to the chat

The ComputablePhrase object returned by the method is detailed below.

#### 7.2.2. `ComputablePhrase`

The `ComputablePhrase` class is the core of this system's formulas. Its purpose is to compute the formulas entered in Labels and Rolls. And it is available in macros !

You can use it like the examples under. For more information, please refer to the [API](https://custom-system-builder.gitlab.io/custom-system-builder/classes/formulas_ComputablePhrase.ComputablePhrase.html).

```javascript
let phrase = new ComputablePhrase("This character's strength is ${str}$ (Mod : ${floor(str-10)/2}$).");
// Computing phrase with only one prop
await phrase.compute({ str: 12 });

// phrase.result returns the fully computed phrase as a String
console.log(phrase.result);
```

```javascript
let phrase = new ComputablePhrase(
  "Rolling for Weapon Skill<br/>Target : ${target:=WS}$<br/>Roll : ${roll:=[1d100]}$ -- ${roll <= target ? (roll % 11 == 0 ? 'Critical success !' : 'Success') : (roll % 11 == 0 ? 'Critical fail !' : 'Fail')}$<br/>Result : ${floor(target/10) - floor(roll/10)}$ SL (${target - roll}$)"
);
// Computing phrase with actor rollData
await phrase.compute(actor.getRollData());

// buildPhrase is a working string in which each formula has been replaced by a unique alphanumeric identifier
let buildPhrase = phrase.buildPhrase;
// values are the computed formulas of the phrase, indexed by the same unique alphanumeric identifier
let values = phrase.values;

// Rebuilding the phrase and setting each computed value bold
for (let key in values) {
  buildPhrase = buildPhrase.replace(key, '<b>' + values[key].result + '</b>');
}

console.log(buildPhrase);
```

## 8. And more

### 8.1. Configure your sheets default display settings

You can configure various display options in the template, with the `Configure sheet display` button.

You can configure the sheet default display width and height, the profile picture size and the header fields placement.

### 8.2. Add complex attribute bars

You can add simple attribute bars by adding Number fields or Meters with a maximum value parameter. But what if your attribute bar's value is automatically calculated ?

Then you just need to click the Configure Attribute Bars button in the template, and add you attribute bar settings there. Each bar must have a Name, a Value and a Maximum. The Value and the Maximum can both be [formulas](#4-formulas)!

> &#x26a0;&#xfe0f; Complex attribute bars are NOT EDITABLE from the Token. If you want editable attribute bars, use number fields with max-values instead (defining one will automatically create an attribute bar for you).

### 8.3. Use Active Effects

Active effects are added to a token by a submenu in the token view. By clicking on an icon, you can add an active effect to the character represented by the token. And now, you can add modifiers triggered by Active Effects!

In the template sheet, click the `Configure active effects` button. From here, a window opens with all the active effects available to be modified. For each active effect, you can set any number of modifier you want, acting the same way as [item modifiers](#52-item-modifiers).

### 8.4. Use your own CSS

You can customize your sheets with your own CSS. You can add a CSS file in the system configuration. The path must lead to a CSS file, starting from Foundry's Data Directory.

### 8.5. Initiative formula

In the system settings, you can set your custom initiative formula. This formula must be formatted like a roll formula, returning only an integer. The `${}$` must not be set around the formula.

### 8.6. Roll visibility

By default, chat messages with rolls will follow FoundryVTT's roll visibility's system, meaning that the messages will always be visible by everyone but

- When Public Roll is selected, the roll & formulas data will be visibile by everyone.
- When Private GM Roll is selected, the roll & formulas data will be visible by the GM and the message's author, other players will see a question mark instead.
- When Blind GM Roll is selected, the roll & formulas data will be visible by the GM only, other players will see a question mark instead.
- When Self Roll is selected, the roll & formulas data will be visible by the author only, other players will see a question mark instead.

In the system's settings, you can expand the roll visibility to the whole messages posted using the sheets. The message will be whispered to its recipients only, and not polluting the chat Log. Players will still be able to show the message to everyone using the right-click > Show to everyone option, and the roll and formulas data will follow the rules above.

### 8.7. Roll Icons

You can add an icon next to rollable labels to make them more visible. To do so, in the system configuration enter a valid [fontawesome](https://fontawesome.com) icon name, like `dice-d20`, to show this icon next to all rollable items.

### 8.8. Diagonal movement configuration

You can configure diagonal movement measurement in the system settings. You have 4 choices for the measurement rule :

- **Equidistant** : Diagonal movement has the same value as straight movement on the scenes
- **Alternating** : Diagonal movement alternates between 1 and 2 times the straight movement (Pathfinder style)
- **Euclidian** : Diagonal movement is calculated as the hypotenuse of the triangle formed with the straight path needed for the movement.
- **Custom** : You can enter a custom multiplier for diagonal movement, which will be applied to the straight movement value.

### 8.9. Export your templates

You can export your templates using the button in the settings tab, and reimport them in another instance using the Import button. This will import every template configuration. If the receiving instance has templates with names existing in the export file, the templates will be overwritten.

### 8.10. Other configurations

#### 8.10.1. Delay entity saving

Activating this setting will disable the default of saving the sheet on each field edition to save only when the user deselects all fields in the sheet. This can be useful if your sheet get large and take some time to reload, especially if you try to input data quickly.

### 8.11. Extend the system

You can extend the system by adding your own component types to it, from a module. To do so, you must create a class inheriting the `Component` class. Two additional abstract classes are present in the base system : `InputComponent`, designed to represent Components which can receive user inputs and `Container`, designed to represent Components holding other Components.

Once your class is developed, you can add it to the system by calling `componentFactory.addComponentType('<componentTechnicalName>', <Class>);` in the Hook `customSystemBuilderInit`.

Example :

```javascript
Hooks.once('customSystemBuilderInit', () => {
  componentFactory.addComponentType('myAwesomeComponent', AwesomeComponent);
});
```

Please refer to [the API for Component implementation](https://custom-system-builder.gitlab.io/custom-system-builder/classes/sheets_components_Component.Component.html).

Please let me know by an issue if you implement a Component you think should be available to all ! Link your implementation, and I'll most likely add it to the system !

## 9. Special Thanks

Thanks to Seregras for the [Sandbox System Builder](https://gitlab.com/rolnl/sandbox-system-builder) which inspired me very much building this system. I hope I will not cause too much trouble to them, after building a "concurrent" system.

Thanks to the FoundryVTT dev community for their help and reactivity on the Discord !

Thanks to Martin1522 for his help on the code :)

Thanks to RafaelC, Graham Wallis, darleth, RoxanneClark, Havoc, Nephilim, Mechanized, Spazzster, gcolgate, Martin1522, Gabryel, Jessica, Nord, Thalsoval, Beico, ephson, Lost Haven Art, Westwiwee, Sazra, Caitlin, Sven Lugar, rjdavis, Matt Beck, coaac, GMofEndessGames, Nathan the Grudgebearer, Katy, Pyram King, TorlanStein, spriggan02, Jeremy, Qwerty Han{Sindar}, Remy Allegro, D.P.HarmAssist, GoozMute, John Pingo, JLaBuck, Nat and Shifu for the coffees !

Special thanks to RomainMoreau, Bear Tow, JerzyRycor, ForgottenLore, Nathan Hanglin, ProjectAvalanche, MuemmelMonsterTV and combatengineerjr for their continued support !
