/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import Container, { ComponentOptions, ContainerJson, ContainerProps } from './Container.js';
import templateFunctions from '../template-functions.js';
import Component, { ComponentJson } from './Component.js';
import { COMPARISON_OPERATOR } from '../../definitions.js';
import Logger from '../../Logger.js';
import { RequiredFieldError } from '../../errors/ComponentValidationError.js';

export type Alignment = 'center' | 'right' | 'left';
/**
 * JSON for the Extensible Tables Components
 */
export type ExtensibleColumnComponentJson = ComponentJson & {
    /** Component Alignment */
    align?: Alignment;
    /** Column name */
    colName?: string;
};

/**
 * Extensible Tables Components additional props
 */
export type ExtensibleColumnProps = {
    /** Component Alignment */
    align?: Alignment;
    /** Column name */
    colName?: string;
};

/**
 * Extensible table props
 */
export type ExtensibleTableProps = ContainerProps & {
    /** Key is mandatory for Extensible Tables */
    key: string;
    /** Should the header row be emphasized? */
    head: boolean;
    /** Extensible Table columns definition */
    rowLayout: Record<string, ExtensibleColumnProps>;
    /** Should a warning be issued on row deletion? */
    deleteWarning: boolean;
};

/**
 * JSON version of an Extensible Table
 */
export type ExtensibleTableJson = ContainerJson & {
    /** Key is mandatory for Extensible Tables */
    key: string;
    /** Should the header row be emphasized? */
    head: boolean;
    /** Extensible Table columns definition */
    rowLayout?: Array<ExtensibleColumnComponentJson>;
    /** Should a warning be issued on row deletion? */
    deleteWarning: boolean;
};

/**
 * Sort options for the table
 */
export type ColumnSortOption = {
    /** Property used for the sort */
    prop?: string;
    /** Direction of the sort */
    operator?: COMPARISON_OPERATOR.GREATER_THAN | COMPARISON_OPERATOR.LESSER_THAN;
    /**Saved order of the rows for manual sorting */
    savedOrder?: Array<string> | Array<number>;
};

/**
 * ExtensibleTable abstract class
 * @abstract
 */
class ExtensibleTable extends Container {
    /**
     * Component key
     */
    protected _key: string;

    /**
     * Component key
     */
    get key(): string {
        return this._key;
    }

    /**
     * Table header should be bold
     */
    protected _head: boolean;

    /**
     * Row layout additional data
     */
    protected _rowLayout: Record<string, ExtensibleColumnProps>;

    /**
     * Display warning on row delete
     */
    protected _deleteWarning: boolean;

    /**
     * Constructor
     * @param props Component data
     */
    constructor(props: ExtensibleTableProps) {
        super(props);
        this._key = props.key;
        this._head = props.head;
        this._rowLayout = props.rowLayout;
        this._deleteWarning = props.deleteWarning;
    }

    /**
     * Component property key
     * @override
     */
    get propertyKey(): string {
        return this.key;
    }

    /**
     * Swaps two dynamic table elements
     * @param entity Rendered entity (actor or item)
     * @param rowIdx1 Index of the first row to swap
     * @param rowIdx2 Index of the second row to swap
     */
    protected _swapElements(entity: TemplateSystem, rowIdx1: number, rowIdx2: number) {
        const tableProps = foundry.utils.getProperty(entity.system.props, this.key);
        const tmpRow = {
            ...tableProps[rowIdx1]
        };

        tableProps[rowIdx1] = tableProps[rowIdx2];
        tableProps[rowIdx2] = tmpRow;

        entity.entity.update({
            system: {
                props: entity.system.props
            }
        });
    }

    /**
     * Deletes a row from the Table
     * @param entity Entity containing the row
     * @param rowIdx Index of the row to delete
     */
    protected async _deleteRow(entity: TemplateSystem, rowIdx: number) {
        const keyPath = 'system.props.' + this.key;
        const tableProps = foundry.utils.getProperty(entity.system.props, this.key);

        if (!(rowIdx in tableProps)) {
            Logger.error('Row index does not exist.');
            return;
        }

        const updateObj = {
            [keyPath]: {
                [`-=${rowIdx}`]: true
            }
        };
        await entity.entity.update(updateObj);
    }

    /**
     * Opens component editor
     * @param entity Rendered entity (actor or item)
     * @param options Component options
     */
    openComponentEditor(entity: TemplateSystem, options: ComponentOptions = {}) {
        // Open dialog to edit new component
        templateFunctions.component(
            (_action: string, component: ComponentJson & ExtensibleColumnComponentJson) => {
                // This is called on dialog validation
                this.addNewComponent(entity, component, options);
            },
            {
                allowedComponents: options.allowedComponents,
                isDynamicTable: true,
                entity
            }
        );
    }

    /**
     * Adds new component to container, handling rowLayout
     * @override
     * @param entity Rendered entity (actor or item)
     * @param component New component
     * @param _options Ignored
     */
    async addNewComponent(
        entity: TemplateSystem,
        component:
            | (ComponentJson & ExtensibleColumnComponentJson)
            | Array<ComponentJson & ExtensibleColumnComponentJson>,
        _options: ComponentOptions = {}
    ) {
        if (!Array.isArray(component)) {
            component = [component];
        }

        for (const aComp of component) {
            if (this._rowLayout[aComp.key!]) {
                throw new Error("Component keys should be unique in the component's columns.");
            }
        }

        for (const aComponent of component) {
            // Add component
            this.contents.push(componentFactory.createOneComponent(aComponent));
            this._rowLayout[aComponent.key!] = {
                align: aComponent.align,
                colName: aComponent.colName
            };
        }

        await this.save(entity);
    }

    /**
     *  @inheritdoc
     */
    replaceComponent(oldComponent: Component & ExtensibleColumnProps, newComponent: Component & ExtensibleColumnProps) {
        super.replaceComponent(oldComponent, newComponent);

        this._rowLayout[newComponent.key!] = {
            align: newComponent.align,
            colName: newComponent.colName
        };

        if (oldComponent.key !== newComponent.key) {
            delete this._rowLayout[oldComponent.key!];
        }
    }

    /**
     * @inheritdoc
     */
    getComponentMap(): Record<string, Component> {
        const componentMap: Record<string, Component> = {};

        if (this.key) {
            componentMap[this.key] = this;
        }

        return componentMap;
    }

    /**
     * @inheritdoc
     */
    getAllProperties(_entity: TemplateSystem): Record<string, string | undefined> {
        const properties: Record<string, string | undefined> = {};

        if (this.propertyKey) {
            properties[this.propertyKey] = undefined;
        }

        return properties;
    }

    /**
     * Returns serialized component
     * @override
     */
    toJSON(): ExtensibleTableJson {
        const jsonObj = super.toJSON();

        const rowLayout: Array<ExtensibleColumnComponentJson> = [];

        for (const component of jsonObj.contents) {
            rowLayout.push({
                ...component,
                align: this._rowLayout?.[component.key!].align ?? 'left',
                colName: this._rowLayout?.[component.key!].colName ?? ''
            });
        }

        return {
            ...jsonObj,
            key: this.key,
            rowLayout: rowLayout,
            head: this._head,
            deleteWarning: this._deleteWarning,
            contents: [],
            type: 'extensibleTable'
        };
    }

    /**
     * Extracts configuration from submitted HTML form
     * @override
     * @param html The submitted form
     * @return The JSON representation of the component
     * @throws {Error} If configuration is not correct
     */
    static extractConfig(html: JQuery): ExtensibleTableJson {
        const superData = super.extractConfig(html) as ContainerJson;

        const fieldData = {
            ...superData,
            key: superData.key ?? '',
            head: html.find('#tableHead').is(':checked'),
            deleteWarning: html.find('#tableDeleteWarning').is(':checked')
        };

        this.validateConfig(fieldData);

        return fieldData;
    }

    static validateConfig(json: ExtensibleTableJson): void {
        super.validateConfig(json);

        if (!json.key) {
            throw new RequiredFieldError('key', json);
        }
    }
}

/**
 * @ignore
 */
export default ExtensibleTable;
