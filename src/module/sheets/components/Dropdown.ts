/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
 * @ignore
 * @module
 */

import InputComponent, { ComponentSize, InputComponentJson, InputComponentProps } from './InputComponent.js';
import { ComponentRenderOptions } from './Component.js';
import { CustomActor } from '../../documents/actor.js';
import Container from './Container.js';
import { ComponentValidationError, RequiredFieldError } from '../../errors/ComponentValidationError.js';

export type DropdownProps = InputComponentProps & {
    selectedOptionType: 'custom' | 'table' | 'formula';
    options: {
        key: string;
        value: string;
    }[];
    tableKey?: string;
    tableKeyColumn?: string;
    tableLabelColumn?: string;
    formulaKeyOptions?: string;
    formulaLabelOptions?: string;
};

export type DropdownJson = InputComponentJson & {
    selectedOptionType?: 'custom' | 'table' | 'formula';
    options: {
        key: string;
        value: string;
    }[];
    tableKey?: string;
    tableKeyColumn?: string;
    tableLabelColumn?: string;
    formulaKeyOptions?: string;
    formulaLabelOptions?: string;
};

/**
 * Dropdown component
 * @ignore
 */
class Dropdown extends InputComponent {
    protected _selectedOptionType: 'custom' | 'table' | 'formula';

    /** Options */
    protected _options: Array<{ key: string; value: string }>;

    /** Dynamic table key */
    protected _tableKey?: string;

    /** Key of the column to use as options keys */
    protected _tableKeyColumn?: string;

    /** Key of the column to use as labels */
    protected _tableLabelColumn?: string;

    /** Formula of the column to use as options keys */
    protected _formulaKeyOptions?: string;

    /** Formula of the column to use as labels */
    protected _formulaLabelOptions?: string;

    /** Dropdown constructor */
    constructor(props: DropdownProps) {
        super(props);
        this._selectedOptionType = props.selectedOptionType;
        this._options = props.options;
        this._tableKey = props.tableKey;
        this._tableKeyColumn = props.tableKeyColumn;
        this._tableLabelColumn = props.tableLabelColumn;
        this._formulaKeyOptions = props.formulaKeyOptions;
        this._formulaLabelOptions = props.formulaLabelOptions;
    }

    get key(): string {
        return this._key!;
    }

    /**
     * Renders component
     * @override
     * @param {TemplateSystem} entity Rendered entity (actor or item)
     * @param {boolean} [isEditable=true] Is the component editable by the current user ?
     * @param {ComponentRenderOptions} [options={}] Additional options usable by the final Component
     * @return {Promise<JQuery>} The jQuery element holding the component
     */
    async _getElement(
        entity: TemplateSystem,
        isEditable: boolean = true,
        options: ComponentRenderOptions = {}
    ): Promise<JQuery> {
        const { reference } = options;

        const jQElement = await super._getElement(entity, isEditable, options);
        jQElement.addClass('custom-system-select');

        const selectElement = $('<select />');
        selectElement.attr('id', `${entity.uuid}-${this.key}`);

        if (!entity.isTemplate) {
            selectElement.attr('name', 'system.props.' + this.key);
        }

        if (!isEditable) {
            selectElement.attr('disabled', 'disabled');
        }

        if (!this.defaultValue) {
            const emptyOption = $('<option></option>');
            emptyOption.attr('value', '');
            selectElement.append(emptyOption);
        }

        const optionKeys: Set<string> = new Set();

        if (!entity.isTemplate) {
            if (this._selectedOptionType === 'table') {
                let baseProps = entity.system.props;
                let tableKey = this._tableKey!;

                if (tableKey.startsWith('parent.')) {
                    baseProps = (entity.entity.parent as CustomActor)?.system.props;
                    tableKey = tableKey.split('.', 2)[1];
                }

                const dynamicProps = foundry.utils.getProperty(baseProps, tableKey);
                if (dynamicProps) {
                    for (const rowIndex in dynamicProps) {
                        if (dynamicProps[rowIndex] && !dynamicProps[rowIndex]?.deleted) {
                            selectElement.append(
                                this._addOption(
                                    optionKeys,
                                    dynamicProps[rowIndex][this._tableKeyColumn!],
                                    this._tableLabelColumn
                                        ? dynamicProps[rowIndex][this._tableLabelColumn]
                                        : dynamicProps[rowIndex][this._tableKeyColumn!]
                                )
                            );
                        }
                    }
                }
            } else if (this._selectedOptionType === 'formula' && !entity.isTemplate) {
                const keyOptions = (
                    await ComputablePhrase.computeMessage(
                        this._formulaKeyOptions!,
                        {
                            ...entity.system.props,
                            ...options.customProps
                        },
                        {
                            ...options,
                            source: `${this.key}.keyOptions`,
                            reference,
                            defaultValue: '',
                            triggerEntity: entity
                        }
                    )
                ).result.split(',');
                const labelOptions = (
                    await ComputablePhrase.computeMessage(
                        this._formulaLabelOptions ?? '',
                        {
                            ...entity.system.props,
                            ...options.customProps
                        },
                        {
                            ...options,
                            source: `${this.key}.labelOptions`,
                            reference,
                            defaultValue: '',
                            triggerEntity: entity
                        }
                    )
                ).result.split(',');

                if (labelOptions[0] !== '' && keyOptions.length !== labelOptions.length) {
                    ui.notifications.error(`${this.key}: Key and Label-options are not of same length`);
                } else {
                    for (let i = 0; i < keyOptions.length; i++) {
                        selectElement.append(
                            this._addOption(
                                optionKeys,
                                keyOptions[i],
                                labelOptions[0] === '' ? keyOptions[i] : labelOptions[i]
                            )
                        );
                    }
                }
            } else {
                for (const option of this._options) {
                    selectElement.append(this._addOption(optionKeys, option.key, option.value));
                }
            }

            const selectedValue =
                foundry.utils.getProperty(entity.system.props, this.key) ??
                ComputablePhrase.computeMessageStatic(this.defaultValue ?? '', entity.system.props, {
                    source: this.key,
                    reference,
                    defaultValue: '',
                    triggerEntity: entity
                }).result;

            selectElement.val(optionKeys.has(selectedValue) ? selectedValue : selectElement.find('option:first').val());
        }

        jQElement.append(selectElement);

        if (entity.isTemplate) {
            jQElement.addClass('custom-system-editable-component');
            selectElement.addClass('custom-system-editable-field');

            jQElement.on('click', (ev) => {
                ev.preventDefault();
                ev.stopPropagation();
                this.editComponent(entity);
            });
        }

        return jQElement;
    }

    /** Returns serialized component */
    toJSON(): DropdownJson {
        const jsonObj = super.toJSON();

        return {
            ...jsonObj,
            selectedOptionType: this._selectedOptionType,
            options: this._options,
            tableKey: this._tableKey,
            tableKeyColumn: this._tableKeyColumn,
            tableLabelColumn: this._tableLabelColumn,
            formulaKeyOptions: this._formulaKeyOptions,
            formulaLabelOptions: this._formulaLabelOptions,
            type: 'select'
        };
    }

    /** Creates Dropdown from JSON description */
    static fromJSON(json: DropdownJson, templateAddress: string, parent: Container): Dropdown {
        return new Dropdown({
            key: json.key,
            tooltip: json.tooltip,
            templateAddress: templateAddress,
            label: json.label,
            defaultValue: json.defaultValue,
            selectedOptionType: json.selectedOptionType ?? 'custom',
            options: json.options,
            tableKey: json.tableKey,
            tableKeyColumn: json.tableKeyColumn,
            tableLabelColumn: json.tableLabelColumn,
            formulaKeyOptions: json.formulaKeyOptions,
            formulaLabelOptions: json.formulaLabelOptions,
            size: json.size,
            cssClass: json.cssClass,
            role: json.role,
            permission: json.permission,
            visibilityFormula: json.visibilityFormula,
            parent: parent
        });
    }

    /**
     * Gets pretty name for this component's type
     * @returns The pretty name
     * @throws {Error} If not implemented
     */
    static getPrettyName(): string {
        return 'Dropdown list';
    }

    /** Get configuration form for component creation / edition */
    static async getConfigForm(existingComponent: DropdownJson, _entity: TemplateSystem): Promise<JQuery> {
        const predefinedValues: Partial<DropdownJson> = { ...existingComponent };
        predefinedValues.selectedOptionType = predefinedValues.selectedOptionType ?? 'custom';

        const mainElt = $('<div></div>');

        mainElt.append(
            await renderTemplate(
                `systems/${game.system.id}/templates/_template/components/dropdown.hbs`,
                predefinedValues
            )
        );

        return mainElt;
    }

    /** Attaches event-listeners to the html of the config-form */
    static attachListenersToConfigForm(html: JQuery) {
        const deleteOptionRow = (event: JQuery.ClickEvent) => {
            const target = $(event.currentTarget);
            const row = target.parents('tr');

            // Remove it from the DOM
            $(row).remove();
        };

        $(html)
            .find('.custom-system-delete-option')
            .on('click', (event) => deleteOptionRow(event));

        $(html)
            .find('#addOption')
            .on('click', (event) => {
                const target = $(event.currentTarget);

                // Last row contains only the add button
                const lastRow = target.parents('tr');

                // Create new row
                const newRow = $(`
<tr class="custom-system-dropdown-option">
    <td>
        <input type="text" class="custom-system-dropdown-option-key" />
    </td>
    <td>
        <input type="text" class="custom-system-dropdown-option-value" />
    </td>
    <td>
        <a class="custom-system-delete-option">
            <i class="fas fa-trash"></i>
        </a>
    </td>
</tr>`);

                $(newRow)
                    .find('.custom-system-delete-option')
                    .on('click', (event) => deleteOptionRow(event));

                // Insert new row before control row
                lastRow.before(newRow);
            });

        $(html)
            .find("input[name='dropdownOptionMode']")
            .on('click', (event) => {
                const target = $(event.currentTarget);
                const customOptions = $('.custom-system-custom-options');
                const dynamicTableOptions = $('.custom-system-dynamic-options');
                const formulaOptions = $('.custom-system-formula-options');
                const slideValue = 200;

                switch (target[0].id) {
                    case 'customOptions':
                        customOptions.slideDown(slideValue);
                        dynamicTableOptions.slideUp(slideValue);
                        formulaOptions.slideUp(slideValue);
                        break;
                    case 'dynamicTableOptions':
                        customOptions.slideUp(slideValue);
                        dynamicTableOptions.slideDown(slideValue);
                        formulaOptions.slideUp(slideValue);
                        break;
                    case 'formulaOptions':
                        customOptions.slideUp(slideValue);
                        dynamicTableOptions.slideUp(slideValue);
                        formulaOptions.slideDown(slideValue);
                        break;
                }
            });
    }

    /**
     * Extracts configuration from submitted HTML form
     * @override
     * @param {JQuery} html The submitted form
     * @return {DropdownJson} The JSON representation of the component
     * @throws {Error} If configuration is not correct
     */
    static extractConfig(html: JQuery): DropdownJson {
        const options: { key: string; value: string }[] = [];
        const selectedOptionType = html.find('#dynamicTableOptions').is(':checked')
            ? 'table'
            : html.find('#formulaOptions').is(':checked')
            ? 'formula'
            : 'custom';

        let tableKey;
        let tableKeyColumn;
        let tableLabelColumn;
        let formulaKeyOptions;
        let formulaLabelOptions;

        switch (selectedOptionType) {
            case 'custom':
                for (const optionRow of html.find('tr.custom-system-dropdown-option')) {
                    const key = $(optionRow).find('.custom-system-dropdown-option-key').val()?.toString() ?? '';
                    let value = $(optionRow).find('.custom-system-dropdown-option-value').val()?.toString() ?? '';

                    if (value === '') {
                        value = key;
                    }

                    options.push({
                        key: key,
                        value: value
                    });
                }
                break;
            case 'table':
                tableKey = html.find('#selectDynamicTableKey').val()?.toString();
                tableKeyColumn = html.find('#selectDynamicTableKeyColumn').val()?.toString();
                tableLabelColumn = html.find('#selectDynamicTableLabelColumn').val()?.toString();
                break;
            case 'formula':
                formulaKeyOptions = html.find('#formulaKeyOptions').val()?.toString();
                formulaLabelOptions = html.find('#formulaLabelOptions').val()?.toString();
                break;
        }

        const fieldData: DropdownJson = {
            ...super.extractConfig(html),
            label: html.find('#selectLabel').val()?.toString(),
            defaultValue: html.find('#selectDefaultValue').val()?.toString(),
            size: (html.find('#selectSize').val()?.toString() as ComponentSize) ?? 'full-size',
            selectedOptionType: selectedOptionType,
            options: options,
            tableKey: tableKey,
            tableKeyColumn: tableKeyColumn,
            tableLabelColumn: tableLabelColumn,
            formulaKeyOptions: formulaKeyOptions,
            formulaLabelOptions: formulaLabelOptions
        };

        this.validateConfig(fieldData);

        return fieldData;
    }

    static validateConfig(json: DropdownJson): void {
        super.validateConfig(json);

        if (!json.key) {
            throw new RequiredFieldError('key', json);
        }

        switch (json.selectedOptionType) {
            case 'custom':
                json.options.forEach((option) => {
                    if (option.key === '') {
                        throw new ComponentValidationError(
                            'Every option in the Dropdown must have a key',
                            'options',
                            json
                        );
                    }
                });
                break;
            case 'formula':
                if (!json.formulaKeyOptions) {
                    throw new RequiredFieldError('formulaKeyOptions', json);
                }
                break;
            case 'table':
                if (!json.tableKey) {
                    throw new RequiredFieldError('tableKey', json);
                }

                if (!json.tableKeyColumn) {
                    throw new RequiredFieldError('tableKeyColumn', json);
                }
                break;
        }
    }

    /**
     * Adds an option to the provided collection
     * @param collection {Set}
     * @param key {String}
     * @param value {String}
     * @returns {JQuery}
     * @private
     */
    _addOption(collection: Set<string>, key: string, value: string): JQuery {
        const optionElement = $('<option></option>');
        collection.add(key);
        optionElement.attr('value', key);
        optionElement.text(value);

        return optionElement;
    }
}

/**
 * @ignore
 */
export default Dropdown;
