/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
 * @ignore
 * @module
 */

import InputComponent, { ComponentSize, InputComponentJson, InputComponentProps } from './InputComponent.js';
import { ComponentRenderOptions, ComponentValueType } from './Component.js';
import Container from './Container.js';
import { RequiredFieldError } from '../../errors/ComponentValidationError.js';

export type CheckboxProps = InputComponentProps & {
    defaultChecked: boolean;
};

export type CheckboxJson = InputComponentJson & {
    defaultChecked: boolean;
};

/**
 * Checkbox component
 * @ignore
 */
class Checkbox extends InputComponent {
    static valueType: ComponentValueType = 'boolean';

    /**
     * Checkbox default state
     */
    protected _defaultChecked: boolean = false;

    /**
     * Checkbox constructor
     */
    constructor(props: CheckboxProps) {
        super(props);

        this._defaultChecked = props.defaultChecked;
    }

    /**
     * Renders component
     * @override
     * @param entity Rendered entity (actor or item)
     * @param isEditable Is the component editable by the current user?
     * @param options Additional options usable by the final Component
     * @returns The jQuery element holding the component
     */
    async _getElement(
        entity: TemplateSystem,
        isEditable: boolean = true,
        options: ComponentRenderOptions = {}
    ): Promise<JQuery> {
        const jQElement = await super._getElement(entity, isEditable, options);
        jQElement.addClass('custom-system-checkbox');

        const inputElement = $('<input />');
        inputElement.attr('type', 'checkbox');
        inputElement.attr('id', `${entity.uuid}-${this.key!}`);

        if (!entity.isTemplate) {
            inputElement.attr('name', 'system.props.' + this.key!);
        }

        const checkedStatus = foundry.utils.getProperty(entity.system.props, this.key!);
        const checked = checkedStatus || (checkedStatus === undefined && this._defaultChecked);

        if (checked) {
            inputElement.attr('checked', 'checked');
        }

        if (!entity.isTemplate) {
            foundry.utils.setProperty(entity.system.props, this.key!, checked);
        }

        if (!isEditable) {
            inputElement.attr('disabled', 'disabled');
        }

        jQElement.append(inputElement);

        if (entity.isTemplate) {
            jQElement.addClass('custom-system-editable-component');
            inputElement.addClass('custom-system-editable-field');

            jQElement.on('click', (ev) => {
                ev.preventDefault();
                ev.stopPropagation();
                this.editComponent(entity);
            });
        }

        return jQElement;
    }

    /**
     * Returns serialized component
     */
    toJSON(): CheckboxJson {
        const jsonObj = super.toJSON();

        return {
            ...jsonObj,
            type: 'checkbox',
            defaultChecked: this._defaultChecked
        };
    }

    /**
     * Creates checkbox from JSON description
     */
    static fromJSON(json: CheckboxJson, templateAddress: string, parent?: Container): Checkbox {
        return new Checkbox({
            key: json.key,
            tooltip: json.tooltip,
            templateAddress: templateAddress,
            label: json.label,
            size: json.size,
            defaultChecked: json.defaultChecked,
            cssClass: json.cssClass,
            role: json.role,
            permission: json.permission,
            visibilityFormula: json.visibilityFormula,
            parent: parent
        });
    }

    /**
     * Gets pretty name for this component's type
     * @returns The pretty name
     * @throws {Error} If not implemented
     */
    static getPrettyName(): string {
        return 'Checkbox';
    }

    /**
     * Get configuration form for component creation / edition
     * @returns The jQuery element holding the component
     */
    static async getConfigForm(existingComponent: CheckboxJson, _entity: TemplateSystem): Promise<JQuery> {
        const mainElt = $('<div></div>');

        mainElt.append(
            await renderTemplate(
                `systems/${game.system.id}/templates/_template/components/checkbox.hbs`,
                existingComponent
            )
        );

        return mainElt;
    }

    /**
     * Extracts configuration from submitted HTML form
     * @param html The submitted form
     * @returns The JSON representation of the component
     * @throws {Error} If configuration is not correct
     */
    static extractConfig(html: JQuery): CheckboxJson {
        const fieldData: CheckboxJson = {
            ...super.extractConfig(html),
            label: html.find('#checkboxLabel').val()?.toString(),
            size: (html.find('#checkboxSize').val()?.toString() as ComponentSize) ?? 'full-size',
            defaultChecked: html.find('#checkboxDefaultChecked').is(':checked')
        };

        this.validateConfig(fieldData);

        return fieldData;
    }

    static validateConfig(json: CheckboxJson): void {
        super.validateConfig(json);

        if (!json.key) {
            throw new RequiredFieldError('key', json);
        }
    }
}

/**
 * @ignore
 */
export default Checkbox;
