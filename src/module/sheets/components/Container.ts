/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import Component, { ComponentJson, ComponentProps, ComponentRenderOptions } from './Component.js';
import templateFunctions from '../template-functions.js';
import InputComponent from './InputComponent.js';
import { updateKeysOnCopy } from '../../utils.js';

/**
 * Container properties
 */
export type ContainerProps = ComponentProps & {
    /** Container contents */
    contents: Array<Component>;
    /** Is it possible to drop Components on this Container ? */
    droppable?: boolean;
};

/**
 * JSON version of a Container
 */
export type ContainerJson = ComponentJson & {
    /** JSONified Container contents */
    contents: Array<ComponentJson>;
};

/**
 * Additional options for Components
 */
export type ComponentOptions = {
    /** Is this component edition in an Extensible Table ? */
    isDynamicTable?: boolean;
    /** List of component types allowed in this Container */
    allowedComponents?: Array<string>;
    /** Rownum of this component - Used in Tables */
    rowNum?: number;
    /** Colnum of this component - Used in Tables */
    colNum?: number;
    /** Before which component to insert the dropped component */
    insertBefore?: Component;
};

/**
 * Abstract container class
 * @abstract
 */
class Container extends Component {
    /**
     * Container contents
     */
    protected _contents: Array<Component>;

    /**
     * Can container accept dropped components ?
     */
    protected _droppable: boolean;

    /**
     * @inheritdoc
     */
    static addWrapperOnTemplate: boolean = true;

    /**
     * Constructor
     */
    constructor(props: ContainerProps) {
        super(props);

        if (this.constructor === Container) {
            throw new TypeError('Abstract class "Container" cannot be instantiated directly');
        }

        this._contents = props.contents;
        this._droppable = props.droppable ?? false;
    }

    /**
     * Container contents
     */
    get contents(): Array<Component> {
        return this._contents;
    }

    /**
     * Can container accept dropped components ?
     */
    get droppable() {
        return this._droppable;
    }

    /**
     * Renders contents component
     * @param entity Rendered entity (actor or item)
     * @param isEditable Is the component editable by the current user ?
     * @param options Additional options usable by the final Component
     * @return The jQuery elements holding the components
     */
    async renderContents(
        entity: TemplateSystem,
        isEditable: boolean = true,
        options: ComponentRenderOptions
    ): Promise<Array<JQuery>> {
        const jqElts = [];

        for (const component of this._contents) {
            jqElts.push(await component.render(entity, isEditable, options));
        }

        return jqElts;
    }

    /**
     * Renders template controls
     * @param entity Rendered entity (actor or item)
     * @param options Component adding options
     * @return The jQuery element holding the component
     */
    async renderTemplateControls(entity: TemplateSystem, options: ComponentOptions = {}): Promise<JQuery> {
        const containerControls = $('<div></div>');
        containerControls.addClass('custom-system-template-tab-controls');

        if (this.droppable) {
            containerControls.addClass('custom-system-droppable-container');

            containerControls
                .on('dragenter', (event) => {
                    event.stopPropagation();
                    event.preventDefault();

                    $('.custom-system-drop-target').remove();
                    containerControls.addClass('custom-system-template-dragged-over');
                })
                .on('dragover', (event) => {
                    event.stopPropagation();
                    event.preventDefault();

                    const sourceId = globalThis.copiedComponent?.sourceId;

                    if (event.originalEvent?.dataTransfer) {
                        if (event.ctrlKey || sourceId !== entity.uuid) {
                            event.originalEvent.dataTransfer.dropEffect = 'copy';
                        } else {
                            event.originalEvent.dataTransfer.dropEffect = 'move';
                        }
                    }
                })
                .on('dragleave', (_event) => {
                    containerControls.removeClass('custom-system-template-dragged-over');
                })
                .on('drop', async (event) => {
                    await this.dropOnComponent(entity, event, this, options);
                });
        }

        const addElement = $('<a></a>');
        addElement.addClass('item custom-system-template-tab-controls-add-element');
        addElement.attr('title', 'Add new element');
        addElement.append('<i class="fas fa-plus-circle custom-system-clickable custom-system-add-component"></i>');

        addElement.on('click', () => {
            this.openComponentEditor(entity, options);
        });

        containerControls.append(addElement);

        return containerControls;
    }

    /**
     * Opens component editor
     * @param entity Rendered entity (actor or item)
     * @param options Component options
     */
    openComponentEditor(entity: TemplateSystem, options: ComponentOptions = {}) {
        let allowedComponents = options.allowedComponents;

        if (allowedComponents) {
            allowedComponents = allowedComponents.filter((value) => entity.allowedComponents.includes(value));
        } else {
            allowedComponents = entity.allowedComponents;
        }

        // Open dialog to edit new component
        templateFunctions.component(
            (_action: string, component: ComponentJson) => {
                // This is called on dialog validation
                this.addNewComponent(entity, component, options);
            },
            { allowedComponents: allowedComponents, entity }
        );
    }

    /**
     * Adds new component to container
     * @param entity Rendered entity (actor or item)
     * @param component JSONified new component
     * @param options Component options
     * @param isMovement Is the new component a moved component (Drag&Drop)
     */
    async addNewComponent(
        entity: TemplateSystem,
        component: ComponentJson | Array<ComponentJson>,
        options: ComponentOptions = {},
        isMovement: boolean = false
    ) {
        if (!Array.isArray(component)) {
            component = [component];
        }

        if (!isMovement) {
            component = updateKeysOnCopy(component, entity.getKeys()) as Array<ComponentJson>;
        }

        let splittingPoint = this._contents.length;

        if (options.insertBefore) {
            const index = this._contents.indexOf(options.insertBefore);
            if (index > -1) {
                splittingPoint = index;
            }
        }

        const firstSlice = this._contents.slice(0, splittingPoint);
        const lastSlice = this._contents.slice(splittingPoint, this._contents.length);

        // Add component
        this._contents = firstSlice.concat(componentFactory.createMultipleComponents(component)).concat(lastSlice);

        await this.save(entity);
    }

    /**
     * Deletes component in the current Container. Does not save the template afterwards.
     * @param component The component to delete
     */
    deleteComponent(component: Component) {
        this._contents = this._contents.filter((elt) => elt !== component);
    }

    /**
     * Replaces component in the current Container. Does not save the template afterwards.
     * @param oldComponent The component to be replaced
     * @param newComponent The component to replace with (Can be a Component Instance or a JSONified Component)
     */
    replaceComponent(oldComponent: Component, newComponent: ComponentJson | Component) {
        const componentIndex = this._contents.indexOf(oldComponent);

        if (!(newComponent instanceof Component)) {
            newComponent = componentFactory.createOneComponent(newComponent, oldComponent.templateAddress, this);
        }

        this._contents[componentIndex] = newComponent as Component;
    }

    /**
     * Sorts component after in the current Container. Does not save the template afterwards.
     * @param component The component to sort
     */
    sortComponentAfter(component: Component) {
        const componentIndex = this._contents.indexOf(component);

        if (componentIndex < this._contents.length - 1 && componentIndex > -1) {
            this._contents[componentIndex] = this._contents[componentIndex + 1];
            this._contents[componentIndex + 1] = component;
        }
    }

    /**
     * Sorts component before in the current Container. Does not save the template afterwards.
     * @param component The component to sort
     */
    sortComponentBefore(component: Component) {
        const componentIndex = this._contents.indexOf(component);

        if (componentIndex > 0) {
            this._contents[componentIndex] = this._contents[componentIndex - 1];
            this._contents[componentIndex - 1] = component;
        }
    }

    /**
     * Go through the contents to get every keyed component in a flat object
     * @override
     */
    getComponentMap(): Record<string, Component> {
        const componentMap = super.getComponentMap();

        for (const component of this.contents) {
            foundry.utils.mergeObject(componentMap, component.getComponentMap());
        }

        return componentMap;
    }

    /**
     * Returns an object of all the component's keys in the Container and their default value
     * @param entity The entity containing the Container
     * @returns The record of all the component's default value
     */
    getAllProperties(entity: TemplateSystem): Record<string, string | undefined> {
        let properties: Record<string, string | undefined> = {};

        if (this.propertyKey) {
            properties[this.propertyKey] = undefined;
        }

        for (const component of this.contents) {
            if (component instanceof Container) {
                properties = {
                    ...properties,
                    ...component.getAllProperties(entity)
                };
            } else {
                if (component.propertyKey) {
                    if (component instanceof InputComponent) {
                        properties = {
                            ...properties,
                            [component.propertyKey]: component.defaultValue
                                ? ComputablePhrase.computeMessageStatic(component.defaultValue, entity.system.props, {
                                      source: `${this.key}.${component.propertyKey}.defaultValue`,
                                      defaultValue: '',
                                      triggerEntity: entity
                                  }).result
                                : undefined
                        };
                    } else {
                        properties[component.propertyKey] = undefined;
                    }
                }
            }
        }

        return properties;
    }

    /**
     * Returns serialized component
     * @override
     */
    toJSON(): ContainerJson {
        const jsonObj = super.toJSON();
        const contentsJSON = [];

        for (const component of this.contents) {
            // Handling Tables, which handle their contents themselves
            if (component instanceof Component) {
                contentsJSON.push(component.toJSON());
            }
        }

        return {
            ...jsonObj,
            contents: contentsJSON,
            type: 'container'
        };
    }
}

/**
 * @ignore
 */
export default Container;
