/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
 * @ignore
 * @module
 */

import Container, { ContainerJson, ContainerProps } from './Container.js';
import Component, { ComponentRenderOptions } from './Component.js';

export type TabProps = ContainerProps & {
    name: string;
};

export type TabJson = ContainerJson & {
    name: string;
};

/**
 * @ignore
 */
class Tab extends Container {
    /** Tab name */
    protected _name: string;

    static addWrapperOnTemplate = false;
    static draggable = false;

    /** Tab constructor */
    constructor(props: TabProps) {
        super(props);
        this._name = props.name;
    }

    get name(): string {
        return this._name;
    }

    get key(): string {
        return this._key!;
    }

    /**
     * Renders component
     * @override
     * @param {TemplateSystem} entity Rendered entity (actor or item)
     * @param {boolean} [isEditable=true] Is the component editable by the current user?
     * @param {ComponentRenderOptions} [options={}]
     * @return {Promise<jQuery>} The jQuery element holding the component
     */
    async _getElement(
        entity: TemplateSystem,
        isEditable: boolean = true,
        options: ComponentRenderOptions = {}
    ): Promise<JQuery> {
        const jQElement = await super._getElement(entity, isEditable, options);

        jQElement.addClass('tab');
        jQElement.attr('data-tab', this.key);
        jQElement.attr('data-group', 'primary');

        const mainPanelElement = $('<div></div>');
        mainPanelElement.addClass('flexcol flex-group-center');

        mainPanelElement.append(await this.renderContents(entity, isEditable, options));

        if (entity.isTemplate) {
            mainPanelElement.append(await this.renderTemplateControls(entity));
        }

        jQElement.append(mainPanelElement);
        return jQElement;
    }

    /**
     * Edits component
     * @param {TemplateSystem} entity Rendered entity (actor or item)
     * @param {Partial<TabJson>} data Diff data
     */
    async update(entity: TemplateSystem, data: Partial<TabJson>): Promise<void> {
        const newComponentJSON = foundry.utils.mergeObject(this.toJSON(), data) as TabJson;

        const newComponent = Tab.fromJSON(newComponentJSON, this.templateAddress, this.parent!);

        this.parent!.replaceComponent(this, newComponent);

        // After actions have been taken care of, save entity
        await this.save(entity);
    }

    /**
     * @inheritdoc
     */
    getComponentMap(): Record<string, Component> {
        const componentMap = {};

        for (const component of this.contents) {
            foundry.utils.mergeObject(componentMap, component.getComponentMap());
        }

        return componentMap;
    }

    /** Returns serialized component */
    toJSON(): TabJson {
        const jsonObj = super.toJSON();

        return {
            ...jsonObj,
            name: this.name,
            type: 'tab'
        };
    }

    /** Creates Tab from JSON description */
    static fromJSON(json: TabJson, templateAddress: string, parent: Container): Tab {
        const tab = new Tab({
            name: json.name,
            key: json.key,
            tooltip: json.tooltip,
            templateAddress: templateAddress,
            contents: [],
            role: json.role,
            permission: json.permission,
            visibilityFormula: json.visibilityFormula,
            parent: parent
        });

        tab._contents = componentFactory.createMultipleComponents(json.contents, templateAddress + '-contents', tab);

        return tab;
    }

    /**
     * Gets pretty name for this component's type
     * @returns The pretty name
     * @throws {Error} If not implemented
     */
    static getPrettyName(): string {
        return 'Tab';
    }
}

/**
 * @ignore
 */
export default Tab;
