/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import TemplateSystemClass from './documents/templateSystem.js';
import ComputablePhraseClass from './formulas/ComputablePhrase.js';
import Component, { ComponentJson } from './sheets/components/Component.js';
import { ComponentFactory } from './sheets/components/ComponentFactory.js';
import { ContainerJson } from './sheets/components/Container.js';

/* eslint-disable no-var */
declare global {
    var TemplateSystem: typeof TemplateSystemClass;
    interface TemplateSystem extends InstanceType<typeof TemplateSystem> {}

    var ComputablePhrase: typeof ComputablePhraseClass;
    interface ComputablePhrase extends InstanceType<typeof ComputablePhrase> {}

    var componentFactory: ComponentFactory;

    var copiedComponent: { sourceId: string; component: Component } | null;

    var DeepDiff: {
        diff: (d1: JSONObject, d2: JSONObject) => Array<JSONObject>;
        applyChange: (target: JSONObject, source: JSONObject, change: JSONObject) => void;
        revertChange: (target: JSONObject, source: JSONObject, change: JSONObject) => void;
    };

    // Type stubs for FoundryV11
    interface Actor {
        system: System;
        flags: JSONObject;
        statuses: Array<string>;
    }
    interface Item {
        system: System;
        flags: JSONObject;
        statuses: Array<string>;
    }

    interface User {
        flags: JSONObject;
    }
    interface TokenDocument {
        flags: JSONObject;
    }
    interface Combat {
        flags: JSONObject;
    }
    interface Combatant {
        flags: JSONObject;
    }
    interface Scene {
        flags: JSONObject;
    }

    interface LenientGlobalVariableTypes {
        game: never;
        ui: never;
    }

    interface CORRECTED_CONST {
        DOCUMENT_OWNERSHIP_LEVELS: Readonly<{
            NONE: 0;
            LIMITED: 1;
            OBSERVER: 2;
            OWNER: 3;
        }>;
    }

    var CORRECTED_CONST: typeof CONST & CORRECTED_CONST;
}

/* eslint-enable no-var */

export type DefinedPrimitive = boolean | number | string;
export type Primitive = boolean | null | number | string | undefined;

export type JSONValue = Primitive | JSONObject | JSONArray;

export interface JSONObject {
    [key: string]: JSONValue;
}

export interface JSONArray extends Array<JSONValue> {}

export interface DeepNestedObject<T> {
    [key: string]: T | DeepNestedObject<T>;
}

export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

/* CSB SPECIFIC TYPES */

export type System = {
    templateSystemUniqueVersion?: number;
    props: JSONObject;
    attributeBar: Record<string, AttributeBar | UnresolvedAttributeBar>;
    hidden: Array<HiddenProp>;
    activeConditionalModifierGroups: Array<string>;
    activeEffects: Record<string, Array<Modifier>>;
    header: JSONObject;
    body: JSONObject;
    unique?: boolean;
    uniqueId?: string;
    display: JSONObject;
    template?: string;
    modifiers?: Array<Modifier>;
};

export type HiddenProp = {
    name: string;
    value: string;
};

export type AttributeBar = {
    key: string,
    max: number;
    value?: number;
    editable: boolean;
};

export type UnresolvedAttributeBar = {
    max: string;
    value?: string | number;
    editable: boolean;
}

export type RollOptions = {
    postMessage?: boolean;
    alternative?: boolean;
};

export type Modifier = {
    id: string;
    conditionalGroup?: string;
    priority: number;
    key: string;
    operator: MODIFIER_OPERATOR;
    formula: string;
    description?: string;
    originalEntity?: TemplateSystem;
    value?: string;
    isSelected?: boolean;
};

export type SortPredicate = {
    prop: string;
    operator: COMPARISON_OPERATOR;
    value?: string;
};

export enum TABLE_SORT_OPTION {
    AUTO = 'auto',
    COLUMN = 'column',
    MANUAL = 'manual',
    DISABLED = 'disabled'
}

export enum COMPARISON_OPERATOR {
    GREATER_THAN = 'gt',
    GREATER_EQUALS = 'geq',
    EQUALS = 'eq',
    NOT_EQUALS = 'neq',
    LESSER_THAN = 'lt',
    LESSER_EQUALS = 'leq',
    FORMULA = 'formula'
}

export enum MODIFIER_OPERATOR {
    SET = 'set',
    ADD = 'add',
    SUBTRACT = 'subtract',
    MULTIPLY = 'multiply',
    DIVIDE = 'divide'
}

export type TableJson = Omit<ContainerJson, 'contents'> & {
    contents: Array<Array<ComponentJson | null>>;
    cols: number;
    rows: number;
    layout?: string;
};
