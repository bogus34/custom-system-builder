import { JSONObject } from '../definitions.js';

export class ComponentValidationError extends Error {
    constructor(message: string, public readonly propertyName: string, public readonly sourceObject: JSONObject) {
        super(message);
        this.name = this.constructor.name;
    }
}

export class RequiredFieldError extends ComponentValidationError {
    constructor(public readonly propertyName: string, public readonly sourceObject: JSONObject) {
        super(`"${propertyName}" must be provided`, propertyName, sourceObject);
    }
}

export class AlphanumericPatternError extends ComponentValidationError {
    constructor(public readonly propertyName: string, public readonly sourceObject: JSONObject) {
        super(`"${propertyName}" must be a string composed of upper and lowercase letters and underscores only`, propertyName, sourceObject);
    }
}

export class NotUniqueError extends ComponentValidationError {
    constructor(public readonly propertyName: string, public readonly sourceObject: JSONObject) {
        super(`"${propertyName}" must be unique`, propertyName, sourceObject);
    }
}

export class NotGreaterThanZeroError extends ComponentValidationError {
    constructor(public readonly propertyName: string, public readonly sourceObject: JSONObject) {
        super(`"${propertyName}" must be greater than 0`, propertyName, sourceObject);
    }
}
