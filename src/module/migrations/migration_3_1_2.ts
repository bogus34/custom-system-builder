/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
 * @ignore
 * @module
 */

import { JSONObject } from '../definitions.js';
import type { CustomActor } from '../documents/actor.js';
import { CustomItem } from '../documents/item.js';
import Logger from '../Logger.js';
import type { ComponentJson } from '../sheets/components/Component.js';
import type { ContainerJson } from '../sheets/components/Container.js';
import { ItemContainerJson } from '../sheets/components/ItemContainer.js';
import type { PanelJson } from '../sheets/components/Panel.js';

async function processMigration() {
    const versionNumber = '3.1.2';

    const actorsToMigrate = game.actors!.filter((actor) =>
        foundry.utils.isNewerVersion(versionNumber, actor.getFlag(game.system.id, 'version') as string)
    ) as CustomActor[];

    if (actorsToMigrate.length === 0) {
        return;
    }

    const templates = actorsToMigrate.filter((document) => document.isTemplate);
    const actors = actorsToMigrate.filter((document) => !document.isTemplate);

    for (let i = 0; i < templates.length; i++) {
        const template = templates[i];

        logProgress(template, versionNumber, i, templates.length);

        template.system.header = updateItemContainersInComponent(template.system.header as PanelJson);
        template.system.body = updateItemContainersInComponent(template.system.body as PanelJson);

        await template.update({
            system: {
                header: template.system.header,
                body: template.system.body,
                templateSystemUniqueVersion: (Math.random() * 0x100000000) >>> 0
            }
        });

        await template.setFlag(game.system.id, 'version', versionNumber);
    }

    for (let i = 0; i < actors.length; i++) {
        const actor = actors[i];

        logProgress(actor, versionNumber, i, actors.length);

        try {
            await actors[i].templateSystem.reloadTemplate();
        } catch (err) {
            Logger.error((err as Error).message, err);
        }

        await actors[i].setFlag(game.system.id, 'version', versionNumber);
    }

    SceneNavigation.displayProgressBar({
        label: 'CSB: Migration finished',
        pct: 100
    });
}

const logProgress = (document: CustomActor | CustomItem, version: string, current: number, total: number): void => {
    Logger.log('Processing migration ' + version + ' for ' + document.name + ' - ' + document.id);
    SceneNavigation.displayProgressBar({
        label: `CSB: Migration to Version ${version}. Updating ${document.constructor.name} ${current + 1} / ${total}`,
        pct: Math.round((current * 100) / total)
    });
};

const updateItemContainersInComponent = (component: ComponentJson): ComponentJson => {
    if (component.type === 'itemContainer') {
        if ((component as JSONObject).itemFilter) {
            const formulaVersion = [];

            for (const filter of (component as JSONObject).itemFilter as Array<Record<string, string>>) {
                let operator;

                switch (filter.operator) {
                    case 'gt':
                        operator = '>';
                        break;
                    case 'geq':
                        operator = '>=';
                        break;
                    case 'lt':
                        operator = '<';
                        break;
                    case 'leq':
                        operator = '<=';
                        break;
                    case 'eq':
                    default:
                        operator = '==';
                        break;
                }

                if (Number.isNumeric(filter.value)) {
                    formulaVersion.push(`item.${filter.prop}${operator}${filter.value}`);
                } else {
                    formulaVersion.push(`equalText(item.${filter.prop}, '${filter.value}')`);
                }
            }

            (component as ItemContainerJson).itemFilterFormula = formulaVersion.join(' and ');
        }
    } else {
        if ((component as ContainerJson).contents) {
            const container: ContainerJson = component as ContainerJson;

            (container.contents as Array<ComponentJson | Array<ComponentJson | undefined>>) = container.contents.map(
                (subComp) => {
                    if (Array.isArray(subComp)) {
                        const tableContents: Array<ComponentJson | undefined> = (subComp as Array<ComponentJson>).map(
                            (subSubComp) => {
                                if (subSubComp) {
                                    return updateItemContainersInComponent(subSubComp);
                                }
                            }
                        );

                        return tableContents;
                    } else {
                        return updateItemContainersInComponent(subComp);
                    }
                }
            );
        }
    }

    return component;
};

export default {
    processMigration
};
