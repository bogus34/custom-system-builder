/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
 * @ignore
 * @module
 */

import { JSONObject } from '../definitions.js';
import { CustomActor } from '../documents/actor.js';
import { CustomItem } from '../documents/item.js';
import { DynamicTableJson } from '../sheets/components/DynamicTable.js';
import { PanelJson } from '../sheets/components/Panel.js';
import {
    finishMigration,
    getActorsToMigrate,
    getComponentKeys,
    getItemsToMigrate,
    reloadTemplatesInDocuments,
    reloadTemplatesInEmbeddedItems,
    updateComponents,
    updateDocuments
} from './migrationUtils.js';

async function processMigration() {
    const versionNumber = '3.2.4';

    const actorsToMigrate = getActorsToMigrate(versionNumber);
    const itemsToMigrate = getItemsToMigrate(versionNumber);

    if (actorsToMigrate.length + itemsToMigrate.length === 0) {
        return;
    }

    const templates = actorsToMigrate.filter((document) => document.isTemplate);
    const actors = actorsToMigrate.filter((document) => !document.isTemplate);
    const itemTemplates = itemsToMigrate.filter((document) => document.isTemplate);
    const items = itemsToMigrate.filter((document) => !document.isTemplate);

    const allEmbeddedItems = actors.map((actor) => Array.from(actor.items)).flat() as Array<CustomItem>;

    await updateDocuments(templates, versionNumber, (template) => {
        return {
            system: {
                header: updateComponents(
                    template.system.header as PanelJson,
                    (component) => component?.type === 'dynamicTable',
                    (component) => updateDynamicTable(component as DynamicTableJson)
                ),
                body: updateComponents(
                    template.system.body as PanelJson,
                    (component) => component?.type === 'dynamicTable',
                    (component) => updateDynamicTable(component as DynamicTableJson)
                ),
                templateSystemUniqueVersion: (Math.random() * 0x100000000) >>> 0
            }
        };
    });

    await reloadTemplatesInDocuments(actors, versionNumber);

    await updateDocuments(itemTemplates, versionNumber, (template) => {
        return {
            system: {
                header: updateComponents(
                    template.system.header as PanelJson,
                    (component) => component?.type === 'dynamicTable',
                    (component) => updateDynamicTable(component as DynamicTableJson)
                ),
                body: updateComponents(
                    template.system.body as PanelJson,
                    (component) => component?.type === 'dynamicTable',
                    (component) => updateDynamicTable(component as DynamicTableJson)
                ),
                templateSystemUniqueVersion: (Math.random() * 0x100000000) >>> 0
            }
        };
    });

    await reloadTemplatesInDocuments(items, versionNumber);
    await reloadTemplatesInEmbeddedItems(actors, versionNumber);

    await updateDynamicTableInDocuments(actors, versionNumber);
    await updateDynamicTableInDocuments(items, versionNumber);
    await updateDynamicTableInDocuments(allEmbeddedItems, versionNumber);

    finishMigration();
}

function updateDynamicTable(component: DynamicTableJson): DynamicTableJson {
    const internalProps = ['deleted', 'predefinedIdx', 'deletionDisabled'];
    const newPredefinedLines = [...(component.predefinedLines ?? [])];

    for (const rowIdx in newPredefinedLines) {
        const row: JSONObject = newPredefinedLines[rowIdx] as JSONObject;
        for (const propKey in row) {
            if (internalProps.includes(propKey)) {
                row[`$${propKey}`] = row[propKey];
                delete row[propKey];
            }
        }
    }

    return {
        ...component,
        predefinedLines: newPredefinedLines
    };
}

async function updateDynamicTableInDocuments<T extends CustomActor | CustomItem>(
    documents: Array<T>,
    versionNumber: string
) {
    await updateDocuments(documents, versionNumber, (document) => {
        const newProps = {};

        console.log('Migrating ' + document.name);

        const allDTableKeys = [
            ...getComponentKeys(document.system.header as PanelJson, (component) => component?.type === 'dynamicTable'),
            ...getComponentKeys(document.system.body as PanelJson, (component) => component?.type === 'dynamicTable')
        ];

        allDTableKeys.forEach((key) => {
            if (document.system.props[key]) {
                foundry.utils.mergeObject(
                    newProps,
                    updateDynamicTableProps(key, document.system.props[key] as JSONObject)
                );
            }
        });

        return { system: { props: newProps } };
    });
}

function updateDynamicTableProps(key: string, props: JSONObject): JSONObject {
    const newObj = { ...props };
    const internalProps = ['deleted', 'predefinedIdx', 'deletionDisabled'];

    for (const rowIdx in newObj) {
        const row: JSONObject = newObj[rowIdx] as JSONObject;
        for (const propKey in row) {
            if (internalProps.includes(propKey)) {
                row[`$${propKey}`] = row[propKey];
                row[`-=${propKey}`] = true;
                delete row[propKey];
            }
        }
    }

    return { [key]: newObj };
}

export default {
    processMigration
};
