import type { CustomActor } from '../documents/actor.js';
import { CustomItem } from '../documents/item.js';
import Logger from '../Logger.js';
import { ComponentJson } from '../sheets/components/Component.js';
import type { ContainerJson } from '../sheets/components/Container.js';
import { JSONObject } from '../definitions.js';
import { ExtensibleTableJson } from '../sheets/components/ExtensibleTable.js';

export function logProgress(document: CustomActor | CustomItem, version: string, current: number, total: number): void {
    Logger.log('Processing migration ' + version + ' for ' + document.name + ' - ' + document.id);
    SceneNavigation.displayProgressBar({
        label: `CSB: Migration to Version ${version}. Updating ${document.constructor.name} ${current + 1} / ${total}`,
        pct: Math.round((current * 100) / total)
    });
}

export function finishMigration(): void {
    SceneNavigation.displayProgressBar({
        label: 'CSB: Migration finished',
        pct: 100
    });
}

export function getActorsToMigrate(versionNumber: string): CustomActor[] {
    return game.actors!.filter((actor) =>
        foundry.utils.isNewerVersion(versionNumber, actor.getFlag(game.system.id, 'version') as string)
    ) as CustomActor[];
}

export function getItemsToMigrate(versionNumber: string): CustomItem[] {
    return game.items!.filter((item) =>
        foundry.utils.isNewerVersion(versionNumber, item.getFlag(game.system.id, 'version') as string)
    ) as CustomItem[];
}

export async function updateDocuments<T extends CustomActor | CustomItem>(
    documents: T[],
    versionNumber: string,
    callback: (document: T) => JSONObject
): Promise<void> {
    for (let i = 0; i < documents.length; i++) {
        const document = documents[i];

        logProgress(document, versionNumber, i, documents.length);

        const diff = callback(document);

        await document.update(diff);
        // @ts-expect-error setFlag is not compatible between actor & item
        await document.setFlag(game.system.id, 'version', versionNumber);
    }
}

export async function reloadTemplatesInEmbeddedItems(actors: CustomActor[], versionNumber: string): Promise<void> {
    const items = actors.flatMap((actor) => Array.from(actor.items));

    for (let i = 0; i < items.length; i++) {
        const item = items[i] as CustomItem;

        logProgress(item, versionNumber, i, items.length);

        try {
            await item.templateSystem.reloadTemplate();
        } catch (err) {
            Logger.error((err as Error).message, err);
        }

        await item.setFlag(game.system.id, 'version', versionNumber);
    }
}

export async function reloadTemplatesInDocuments<T extends CustomActor | CustomItem>(
    documents: T[],
    versionNumber: string
): Promise<void> {
    for (let i = 0; i < documents.length; i++) {
        const document = documents[i];

        logProgress(document, versionNumber, i, documents.length);

        try {
            await document.templateSystem.reloadTemplate();
        } catch (err) {
            Logger.error((err as Error).message, err);
        }

        // @ts-expect-error setFlag is not compatible between actor & item
        await document.setFlag(game.system.id, 'version', versionNumber);
    }
}

export function updateComponents(
    component: ComponentJson,
    predicate: (component?: ComponentJson) => boolean,
    callback: (component: ComponentJson) => ComponentJson
): ComponentJson {
    if (predicate(component)) {
        component = callback(component);
    }

    if ((component as ContainerJson)?.contents) {
        const container = component as ContainerJson;

        (container.contents as Array<ComponentJson | Array<ComponentJson | undefined>>) = container.contents.map(
            (subComp) => {
                if (Array.isArray(subComp)) {
                    const tableContents: Array<ComponentJson | undefined> = (subComp as Array<ComponentJson>).map(
                        (subSubComp) => {
                            if (subSubComp) {
                                return updateComponents(subSubComp, predicate, callback);
                            }
                        }
                    );

                    return tableContents;
                } else {
                    return updateComponents(subComp, predicate, callback);
                }
            }
        );
    }

    if ((component as ExtensibleTableJson)?.rowLayout) {
        const extensibleTable = component as ExtensibleTableJson;

        extensibleTable.rowLayout = extensibleTable.rowLayout?.map((subComp) => {
            return updateComponents(subComp, predicate, callback);
        });
    }

    return component;
}

export function getComponentKeys(
    component: ComponentJson,
    predicate: (component?: ComponentJson) => boolean
): Set<string> {
    let allKeys = new Set<string>();
    if (component.key && predicate(component)) {
        allKeys.add(component.key);
    }

    if ((component as ContainerJson)?.contents) {
        const container = component as ContainerJson;

        container.contents.forEach((subComp) => {
            if (Array.isArray(subComp)) {
                (subComp as Array<ComponentJson>).forEach((subSubComp) => {
                    if (subSubComp) {
                        allKeys = new Set([...allKeys, ...getComponentKeys(subSubComp, predicate)]);
                    }
                });
            } else {
                allKeys = new Set([...allKeys, ...getComponentKeys(subComp, predicate)]);
            }
        });
    }

    if ((component as ExtensibleTableJson)?.rowLayout) {
        const extensibleTable = component as ExtensibleTableJson;

        extensibleTable.rowLayout?.forEach((subComp) => {
            allKeys = new Set([...allKeys, ...getComponentKeys(subComp, predicate)]);
        });
    }

    return allKeys;
}
